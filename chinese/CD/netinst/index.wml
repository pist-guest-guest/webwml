#use wml::debian::cdimage title="从最小 USB 或 CD 进行网络安装"
#use wml::debian::release_info
#use wml::debian::installer
#use wml::debian::translation-check translation="678a572e92767d98d57f67822e00905782bf9637"
#include "$(ENGLISHDIR)/releases/images.data"

<div class="tip">
<p>在 i386 和 amd64 架构上，所有 USB/CD/DVD 映像也可以\
<a href="https://www.debian.org/CD/faq/#write-usb">在 [CN:U 盘:][HK:USB 手指:][TW:USB 随身碟:]上使用</a>。</div>

<p><q>网络安装</q>或<q>netinst CD</q>是单张光盘，可以\
安装整个操作系统。 这张光盘只包含\
最少的软件以安装基本系统，并\
通过因特网获取剩余的软件包。</p>

<p><strong>在安装过程中支持哪些类型的\
网络连接？</strong>
支持各种不同的方式，如\
以太网和 WLAN（有一些限制）。</p>

<p>以下最小可启动光盘映像可供\
下载：</p>

<ul>

  <li><q>稳定（stable）</q>版本的官方<q>网络安装</q>映像 &mdash; <a
  href="#netinst-stable">见下</a></li>

  <li><q>测试（testing）</q>版本的映像 &mdash; 请参见 <a
  href="$(DEVEL)/debian-installer/">Debian 安装程序页面</a>。</li>

</ul>


<h2 id="netinst-stable"><q>稳定（stable）</q>版本的官方网络安装映像</h2>

<p>该映像包含安装程序及一\
小组可以安装（非常）基本系统\
的软件包。</p>

<div class="line">
<div class="item col50">
<p><strong>网络安装 CD 映像</strong></p>
	  <stable-netinst-images />
</div>
<div class="item col50 lastcol">
<p><strong>网络安装 CD 映像（通过 <a href="$(HOME)/CD/torrent-cd">bittorrent</a>）</strong></p>
	  <stable-netinst-torrent />
</div>
<div class="clear"></div>
</div>

<p>有关这些文件是什么，以及如何使用这些文件，请\
参阅 <a href="../faq/">FAQ</a>。</p>

<p>一旦您已下载映像，请确保查看\
<a href="$(HOME)/releases/stable/installmanual">有关\
安装过程的详细信息</a>。</p>
