#use wml::debian::translation-check translation="9d41ab1625a3bbe9bf95b782d91e91b766a3f664" maintainer="Erik Pfannenstein"
<define-tag pagetitle>Debian 11 aktualisiert: 11.9 veröffentlicht</define-tag>
<define-tag release_date>2024-02-10</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>11</define-tag>
<define-tag codename>Bullseye</define-tag>
<define-tag revision>11.9</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Das Debian-Projekt freut sich, die neunte Aktualisierung seiner Oldstable-Distribution
Debian <release> (Codename <q><codename></q>) ankündigen zu dürfen. Diese
Zwischenveröffentlichung behebt hauptsächlich Sicherheitslücken der Oldstable-Veröffentlichung
sowie einige ernste Probleme. Es sind bereits separate Sicherheitsankündigungen
veröffentlicht worden, auf die, wenn möglich, verwiesen wird.
</p>

<p>
Bitte beachten Sie, dass diese Zwischenveröffentlichung keine neue Version von
Debian <release> darstellt, sondern nur einige der enthaltenen Pakete auffrischt.
Es gibt keinen Grund, <q><codename></q>-Medien zu entsorgen, da deren Pakete
auch nach der Installation durch einen aktualisierten Debian-Spiegelserver auf
den neuesten Stand gebracht werden können.
</p>

<p>Wer häufig Aktualisierungen von security.debian.org herunterlädt, wird nicht viele
Pakete auf den neuesten Stand bringen müssen. Die meisten dieser Aktualisierungen sind
in dieser Revision enthalten.</p>

<p>Neue Installationsabbilder können bald von den gewohnten Orten bezogen werden.</p>

<p>Vorhandene Installationen können auf diese Revision angehoben werden, indem
das Paketverwaltungssystem auf einen der vielen HTTP-Spiegel von Debian verwiesen
wird. Eine vollständige Liste der Spiegelserver ist verfügbar unter:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Verschiedene Fehlerbehebungen</h2>

<p>Diese Oldstable-Aktualisierung fügt den folgenden Paketen einige wichtige Korrekturen hinzu:</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction axis "Nicht unterstützte Protokolle in der Client-Klasse ServiceFactory ausfiltern [CVE-2023-40743]">
<correction base-files "Aktualisiert für die Zwischenveröffentlichung 11.9">
<correction cifs-utils "Nicht-parallele Kompilierungen überarbeitet">
<correction compton "Picom-Empfehlung entfernt">
<correction conda-package-handling "Unzuverlässige Tests überspringen">
<correction conmon "Beim Weiterleiten der Container-Standard-(Fehler-)-Ausgabe nicht hängen bleiben, wenn viel Text durchläuft">
<correction crun "Container mit systemd als Init-System auf neueren Kernel-Versionen überarbeitet">
<correction debian-installer "Linux-Kernel-ABI auf 5.10.0-28 angehoben; Neukompilierung gegen proposed-updates">
<correction debian-installer-netboot-images "Neukompilierung gegen proposed-updates">
<correction debian-ports-archive-keyring "Debian Ports Archive Automatic Signing Key (2025) hinzugefügt">
<correction debian-security-support "tor, consul und xen als EOL markiert; samba-Unterstützung für Nicht-AD-Anwendungsfälle eingeschränkt; golang-Pakete mit regulärem Ausdruck abgleichen; versionsbasierte Prüfung abgeschafft; chromium zu security-support-ended.deb11 hinzugefügt; tiles und libspring-java zu security-support-limited hinzugefügt">
<correction debootstrap "Änderungen an der Unterstützung für merged-/usr aus Trixie zurückportiert: merged-/usr durch post-merging implementiert, standardmäßig in allen Profilen merged-/usr für Suites verwenden, die neuer als Bookworm sind">
<correction distro-info "Tests für distro-info-data 0.58+deb12u1 hinzugefügt, welches das Lebensendedatum von Debian 7 geändert hat">
<correction distro-info-data "Ubuntu 24.04 LTS Noble Numbat hinzugefügt; mehrere Lebensende-Daten korrigiert">
<correction dpdk "Neue stabile Version der Originalautoren">
<correction dropbear "Umgehungsmöglichkeit für Sicherheitsmaßnahmen geschlossen [CVE-2021-36369]; Abwehr des <q>Terrapin</q>-Angriffs [CVE-2023-48795]">
<correction exuberant-ctags "Eigenmächtige Codeausführung abgestellt [CVE-2022-4515]">
<correction filezilla "<q>Terrapin</q>-Exploit blockiert [CVE-2023-48795]">
<correction gimp "Alte Versionen des separat paktierten dds-Plugins entfernt">
<correction glib2.0 "Angleichung an stabile Korrekturen der Originalautoren; Dienstblockade unterbunden [CVE-2023-32665 CVE-2023-32611 CVE-2023-29499 CVE-2023-32636]">
<correction glibc "Speicherkorrumpierung bei <q>qsort()</q> bei Verwendung nicht-transitiver Vergleichsfunktionen behoben">
<correction gnutls28 "Sicherheitskorrektur für Timing-Seitenkanalangriff [CVE-2023-5981]">
<correction imagemagick "Verschiedene Sicherheitskorrekturen [CVE-2021-20241 CVE-2021-20243 CVE-2021-20244 CVE-2021-20245 CVE-2021-20246 CVE-2021-20309 CVE-2021-3574 CVE-2021-39212 CVE-2021-4219 CVE-2022-1114 CVE-2022-28463 CVE-2022-32545 CVE-2022-32546]">
<correction jqueryui "Anfälligkeit für seitenübergreifendes Skripting behoben [CVE-2022-31160]">
<correction knewstuff "Auf richtige ProvidersUrl achten, um Dienstblockade vorzubeugen">
<correction libdatetime-timezone-perl "Enthaltene Zeitzonendaten aktualisiert">
<correction libde265 "Segmentierungsverletzung in Funktion <q>decoder_context::process_slice_segment_header</q> behoben [CVE-2023-27102]; Pufferüberlauf in der Funktion <q>derive_collocated_motion_vectors</q> behoben [CVE-2023-27103]; übermäßiges Pufferlesen in <q>pic_parameter_set::dump</q> unterbunden [CVE-2023-43887]; Pufferüberlauf in <q>slice_segment_header</q>-Funktion abgestellt [CVE-2023-47471]; Pufferüberläufe behoben [CVE-2023-49465 CVE-2023-49467 CVE-2023-49468]">
<correction libmateweather "Enthaltene Lokationsdaten aktualisiert; Datenserver-URL aktualisiert">
<correction libpod "Falschen Umgang mit zusätzlichen Gruppen korrigiert [CVE-2022-2989]">
<correction libsolv "Unterstützung für zstd-Komprimierung aktiviert">
<correction libspreadsheet-parsexlsx-perl "Mögliche Speicherbombe entschärft [CVE-2024-22368]; XML External Entity behoben [CVE-2024-23525]">
<correction linux "Neue stabile Version der Originalautoren; ABI auf 28 angehoben">
<correction linux-signed-amd64 "Neue stabile Version der Originalautoren; ABI auf 28 angehoben">
<correction linux-signed-arm64 "Neue stabile Version der Originalautoren; ABI auf 28 angehoben">
<correction linux-signed-i386 "Neue stabile Version der Originalautoren; ABI auf 28 angehoben">
<correction llvm-toolchain-16 "Neues zurückportiertes Paket, um Kompilierungen neuerer chromium-Versionen zu unterstützen; Kompilierabhängigkeit von <q>llvm-spirv</q> statt <q>llvm-spirv-16</q>">
<correction mariadb-10.5 "Neue stabile Version der Originalautoren; Dienstblockade behoben [CVE-2023-22084]">
<correction minizip "Übergelaufene ZIP-Kopfzeilenfelder abweisen [CVE-2023-45853]">
<correction modsecurity-apache "Schutzumgehungen behoben [CVE-2022-48279 CVE-2023-24021]">
<correction nftables "Fehlerhafte Bytecode-Generierung überarbeitet">
<correction node-dottie "Protoype Pollution behoben [CVE-2023-26132]">
<correction node-url-parse "Autorisierungsumgehung behoben [CVE-2022-0512]">
<correction node-xml2js "Protoype Pollution behoben [CVE-2023-0842]">
<correction nvidia-graphics-drivers "Neue Version der Originalautoren [CVE-2023-31022]">
<correction nvidia-graphics-drivers-tesla-470 "Neue Version der Originalautoren [CVE-2023-31022]">
<correction opendkim "Authentication-Results-Kopfzeilen richtig löschen [CVE-2022-48521]">
<correction perl "Pufferüberlauf durch illegale Unicode-Eigenschaft abgestellt [CVE-2023-47038]">
<correction plasma-desktop "Dienstblockade-Fehler in discover behoben">
<correction plasma-discover "Dienstblockade-Fehler behoben; Kompilierungsfehlschlag behoben">
<correction postfix "Neue stabile Version der Originalautoren; SMTP-Schmuggel angegangen [CVE-2023-51764]">
<correction postgresql-13 "Neue stabile Version der Originalautoren; SQL-Injektion behoben [CVE-2023-39417]">
<correction postgresql-common "autopkgtests korrigiert">
<correction python-cogent "Parallele Tests auf Einzel-CPU-Systemen überspringen">
<correction python-django-imagekit "Vermeiden, dass die Verzeichnisüberschreitungs-Erkennung beim Testen anschlägt">
<correction python-websockets "Vorhersehbare Zeitverzögerungen behoben [CVE-2021-33880]">
<correction pyzoltan "Auf Einzel-Kern-Systemen kompilieren">
<correction ruby-aws-sdk-core "VERSION-Datei in Paket enthalten">
<correction spip "Seitenübergreifendes Skripting behoben">
<correction swupdate "Erschleichen von root-Rechten durch unzutreffenden Socket-Modus abgestellt">
<correction symfony "Sichergestellt, dass die Filter von CodeExtension ihre Eingaben richtig maskieren [CVE-2023-46734]">
<correction tar "Begrenzungsprüfung im base-256-Decoder [CVE-2022-48303], Handhabung erweiterter Kopfzeilen-Präfixe [CVE-2023-39804] überarbeitet">
<correction tinyxml "Problem mit Assertion behoben [CVE-2023-34194]">
<correction tzdata "Enthaltene Zeitzonendaten aktualisiert">
<correction unadf "Stack-Pufferüberlauf abgestellt [CVE-2016-1243]; eigenmächtige Codeausführung unterbunden [CVE-2016-1244]">
<correction usb.ids "Enthaltene Datenliste aktualisiert">
<correction vlfeat "FTBFS bei neuerem ImageMagick korrigiert">
<correction weborf "Dienstblockade behoben">
<correction wolfssl "Pufferüberlauf [CVE-2022-39173 CVE-2022-42905], Schlüsseloffenlegung [CVE-2022-42961], vorhersehbare Puffer im Input Keying Material [CVE-2023-3724] korrigiert">
<correction xerces-c "Use-after-free abgestellt [CVE-2018-1311]; Ganzzahlüberlauf behoben [CVE-2023-37536]">
<correction zeromq3 "<q>fork()</q>-Erkennung mit gcc 7 überarbeitet; Stellungnahme wegen Relizenzierung aktualisiert">
</table>

<h2>Sicherheitsaktualisierungen</h2>

<p>Diese Revision nimmt an der Oldstable-Veröffentlichung die folgenden
Sicherheitsaktualisierungen vor. Das Sicherheitsteam hat bereits für
jede davon eine Ankündigung veröffentlicht:</p>

<table border=0>
<tr><th>Ankündigungs-ID</th>  <th>Paket</th></tr>
<dsa 2023 5496 firefox-esr>
<dsa 2023 5499 chromium>
<dsa 2023 5506 firefox-esr>
<dsa 2023 5508 chromium>
<dsa 2023 5509 firefox-esr>
<dsa 2023 5511 mosquitto>
<dsa 2023 5512 exim4>
<dsa 2023 5513 thunderbird>
<dsa 2023 5514 glibc>
<dsa 2023 5515 chromium>
<dsa 2023 5516 libxpm>
<dsa 2023 5517 libx11>
<dsa 2023 5518 libvpx>
<dsa 2023 5519 grub-efi-amd64-signed>
<dsa 2023 5519 grub-efi-arm64-signed>
<dsa 2023 5519 grub-efi-ia32-signed>
<dsa 2023 5519 grub2>
<dsa 2023 5520 mediawiki>
<dsa 2023 5522 tomcat9>
<dsa 2023 5523 curl>
<dsa 2023 5524 libcue>
<dsa 2023 5526 chromium>
<dsa 2023 5527 webkit2gtk>
<dsa 2023 5528 node-babel7>
<dsa 2023 5530 ruby-rack>
<dsa 2023 5531 roundcube>
<dsa 2023 5533 gst-plugins-bad1.0>
<dsa 2023 5534 xorg-server>
<dsa 2023 5535 firefox-esr>
<dsa 2023 5536 chromium>
<dsa 2023 5537 openjdk-11>
<dsa 2023 5538 thunderbird>
<dsa 2023 5539 node-browserify-sign>
<dsa 2023 5540 jetty9>
<dsa 2023 5542 request-tracker4>
<dsa 2023 5543 open-vm-tools>
<dsa 2023 5544 zookeeper>
<dsa 2023 5545 vlc>
<dsa 2023 5546 chromium>
<dsa 2023 5547 pmix>
<dsa 2023 5548 openjdk-17>
<dsa 2023 5549 trafficserver>
<dsa 2023 5550 cacti>
<dsa 2023 5551 chromium>
<dsa 2023 5554 postgresql-13>
<dsa 2023 5556 chromium>
<dsa 2023 5557 webkit2gtk>
<dsa 2023 5558 netty>
<dsa 2023 5560 strongswan>
<dsa 2023 5561 firefox-esr>
<dsa 2023 5563 intel-microcode>
<dsa 2023 5564 gimp>
<dsa 2023 5565 gst-plugins-bad1.0>
<dsa 2023 5566 thunderbird>
<dsa 2023 5567 tiff>
<dsa 2023 5569 chromium>
<dsa 2023 5570 nghttp2>
<dsa 2023 5571 rabbitmq-server>
<dsa 2023 5572 roundcube>
<dsa 2023 5573 chromium>
<dsa 2023 5574 libreoffice>
<dsa 2023 5576 xorg-server>
<dsa 2023 5577 chromium>
<dsa 2023 5579 freeimage>
<dsa 2023 5581 firefox-esr>
<dsa 2023 5582 thunderbird>
<dsa 2023 5584 bluez>
<dsa 2023 5585 chromium>
<dsa 2023 5586 openssh>
<dsa 2023 5587 curl>
<dsa 2023 5588 putty>
<dsa 2023 5590 haproxy>
<dsa 2023 5591 libssh>
<dsa 2023 5592 libspreadsheet-parseexcel-perl>
<dsa 2024 5594 linux-signed-amd64>
<dsa 2024 5594 linux-signed-arm64>
<dsa 2024 5594 linux-signed-i386>
<dsa 2024 5594 linux>
<dsa 2024 5595 chromium>
<dsa 2024 5597 exim4>
<dsa 2024 5598 chromium>
<dsa 2024 5599 phpseclib>
<dsa 2024 5600 php-phpseclib>
<dsa 2024 5602 chromium>
<dsa 2024 5603 xorg-server>
<dsa 2024 5604 openjdk-11>
<dsa 2024 5605 thunderbird>
<dsa 2024 5606 firefox-esr>
<dsa 2024 5608 gst-plugins-bad1.0>
<dsa 2024 5613 openjdk-17>
<dsa 2024 5614 zbar>
<dsa 2024 5615 runc>
</table>

<h2>Entfernte Pakete</h2>

<p>Das folgende obsolete Paket wurde aus der Distribution entfernt:</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction gimp-dds "Integriert in gimp >=2.10">

</table>

<h2>Debian-Installer</h2>

<p>Der Installer wurde aktualisiert, damit er die Korrekturen enthält,
die mit dieser Zwischenveröffentlichung in Oldstable eingeflossen sind.</p>

<h2>URLs</h2>

<p>Die vollständige Liste von Paketen, die sich mit dieser Revision geändert haben:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>


<p>Die derzeitige Oldstable-Distribution:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable/">
</div>

<p>Vorgeschlagene Aktualisierungen für die Oldstable-Distribution:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>Informationen zur Oldstable-Distribution (Veröffentlichungshinweise, Errata usw.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>Sicherheitsankündigungen und -informationen:</p>


<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Über Debian</h2>

<p>Das Debian-Projekt ist ein Zusammenschluss von Entwicklern
Freier Software, die ihre Zeit und Mühen einbringen, um das
vollständig freie Betriebssystem Debian zu erschaffen.</p>

<h2>Kontaktinformationen</h2>

<p>Für weitere Informationen besuchen Sie bitte die Debian-Website unter
<a href="$(HOME)/">https://www.debian.org/</a>, schicken Sie eine Mail
(auf Englisch) an &lt;press@debian.org&gt; oder kontaktieren Sie das
Stable-Veröffentlichungs-Team (auf Englisch)
unter &lt;debian-release@lists.debian.org&gt;.</p>

