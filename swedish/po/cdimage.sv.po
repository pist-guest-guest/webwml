#
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: (null)\n"
"PO-Revision-Date: 2024-09-16 16:25+0200\n"
"Last-Translator: Andreas Rönnquist <andreas@ronnquist.net>\n"
"Language-Team: Swedish <debian-l10n-swedish@lists.debian.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 3.4.4\n"

#. Translators: string printed by gpg --fingerprint in your language, including spaces
#: ../../english/CD/CD-keys.data:4 ../../english/CD/CD-keys.data:8
#: ../../english/CD/CD-keys.data:12
msgid "      Key fingerprint"
msgstr "      Nyckelns fingeravtryck"

#: ../../english/devel/debian-installer/images.data:89
msgid "ISO images"
msgstr "ISO-avbildningar"

#: ../../english/devel/debian-installer/images.data:90
msgid "Jigdo files"
msgstr "Jigdo-filer"

#. note: only change the sep(arator) if it's not good for your charset
#: ../../english/template/debian/cdimage.wml:10
msgid "&middot;"
msgstr "&middot;"

#: ../../english/template/debian/cdimage.wml:13
msgid "<void id=\"dc_faq\" />FAQ"
msgstr "<void id=\"dc_faq\" />FAQ"

#: ../../english/template/debian/cdimage.wml:16
msgid "Download with Jigdo"
msgstr "Hämta med jigdo"

#: ../../english/template/debian/cdimage.wml:19
msgid "Download via HTTP/FTP"
msgstr "Hämta via http/ftp"

#: ../../english/template/debian/cdimage.wml:22
msgid "Buy CDs or DVDs"
msgstr "Köp cd eller dvd"

#: ../../english/template/debian/cdimage.wml:25
msgid "Network Install"
msgstr "Nätverksinstallation"

#: ../../english/template/debian/cdimage.wml:28
msgid "<void id=\"dc_download\" />Download"
msgstr "<void id=\"dc_download\" />Hämta"

#: ../../english/template/debian/cdimage.wml:31
msgid "<void id=\"dc_misc\" />Misc"
msgstr "<void id=\"dc_misc\" />Diverse"

#: ../../english/template/debian/cdimage.wml:34
msgid "<void id=\"dc_artwork\" />Artwork"
msgstr "<void id=\"dc_artwork\" />Bilder"

#: ../../english/template/debian/cdimage.wml:37
msgid "<void id=\"dc_mirroring\" />Mirroring"
msgstr "<void id=\"dc_mirroring\" />Spegla"

#: ../../english/template/debian/cdimage.wml:40
msgid "<void id=\"dc_rsyncmirrors\" />Rsync Mirrors"
msgstr "<void id=\"dc_rsyncmirrors\" />Rsync-speglar"

#: ../../english/template/debian/cdimage.wml:43
msgid "<void id=\"dc_verify\" />Verify"
msgstr "<void id=\"dc_verify\" />Verifiera"

#: ../../english/template/debian/cdimage.wml:46
msgid "<void id=\"dc_torrent\" />Download with Torrent"
msgstr "<void id=\"dc_torrent\" />Hämta med Torrent"

#: ../../english/template/debian/cdimage.wml:49
msgid "<void id=\"dc_live\" />Live images"
msgstr "<void id=\"dc_live\" />Liveavbildningar"

#: ../../english/template/debian/cdimage.wml:52
msgid "Debian CD team"
msgstr "Debians cd-grupp"

#: ../../english/template/debian/cdimage.wml:55
msgid "debian_on_cd"
msgstr "debian på cd"

#: ../../english/template/debian/cdimage.wml:58
msgid "<void id=\"faq-bottom\" />faq"
msgstr "<void id=\"faq-bottom\" />osf"

#: ../../english/template/debian/cdimage.wml:61
msgid "jigdo"
msgstr "jigdo"

#: ../../english/template/debian/cdimage.wml:64
msgid "http_ftp"
msgstr "http/ftp"

#: ../../english/template/debian/cdimage.wml:67
msgid "buy"
msgstr "köp"

#: ../../english/template/debian/cdimage.wml:70
msgid "net_install"
msgstr "nätinstall"

#: ../../english/template/debian/cdimage.wml:73
msgid "<void id=\"misc-bottom\" />misc"
msgstr "<void id=\"misc-bottom\" />diverse"

#: ../../english/template/debian/cdimage.wml:76
msgid ""
"English-language <a href=\"/MailingLists/disclaimer\">public mailing list</"
"a> for CDs/DVDs:"
msgstr ""
"Engelskspråkig <a href=\"/MailingLists/disclaimer\">öppen sändlista</a> för "
"cd/dvd:"

#~ msgid "<void id=\"dc_relinfo\" />Image Release Info"
#~ msgstr "Utgåveinformation för avbildningar"
