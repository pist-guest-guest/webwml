#use wml::debian::template title="Information för Debiankonsulter" MAINPAGE="true"
#use wml::debian::translation-check translation="5f70081cb38037d61262d8cb159fb510c9315981"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#policy">Policy för Debians Konsultsida</a></li>
  <li><a href="#change">Tillägg, ändringar och borttagning av poster</a></li>
</ul>


<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Konsulter kan kommunicera med andra Debiankonsulter via <a href="https://lists.debian.org/debian-consultants/">sändlistan debian-consultants</a>. 
Denna lista modereras inte; alla kan posta där, Sändlistan tillhandahåller även ett publikt arkiv.</p>
</aside>

<h2><a id="policy">Policy för Debians Konsultsida</a></h2>


<p>
Om du vill stå med som en konsult på Debians webbplats ber vi dig beakta
följande regler:
</p>

<ul>
 <li><strong>Obligatoriska kontaktuppgifter:</strong><br />
   Du måste tillhandahålla en fungerande e-postadress och besvara e-post som
   sänds till dig inom åtminstone fyra veckor. För att förhindra missbruk, måste varje ny förfrågan (tillägg,
   borttagningar och ändringar) sändas från samma adress. För att undvika
   skräppost så kan du få din e-postadress förvrängd (t.ex. <em>debian -punkt-
   consulting -at- exempel -punkt- se</em>). Om du efterfrågar detta, vänligen
   notera det uttryckligen i din ansökan. Alternativt kan du efterfråga att
   din e-postadress inte alls är synlig på webbsidan (vi behöver dock en giltig
   e-postadress för underhåll av listan). Om du vill ha din email gömd på listan,
   så kan du istället tillhandahålla oss en URL till ett kontaktformulär på din
   webbsida som vi kan länka till. Fältet <q>Contact</q> är tänkt att användas
   för detta.
 </li>
 <li><strong>Din webbplats:</strong><br />
  Om du tillhandahåller en länk till en webbplats, måste platsen nämna dina
  konsulttjänster kring Debian. Att tillhandahålla direktlänken istället för
  bara hemsidan är inte obligatoriskt om informationen går att hitta utan
  besvär, men är mycket uppskattat.
 </li>
 <li><strong>Flera städer/regioner/länder:</strong><br />
  Varje konsult måste välja i vilket land (endast ett) de vill stå uppförda.
  Ytterligare städer, regioner eller länder kan listan antingen som del
  av ytterligare information, eller på din egen webbplats.
 </li>
 <li><strong>Regler för Debianutvecklare:</strong><br />
  Det är inte tillåtet att använda din officiella <em>@debian.org</em>-adress i
  konsultförteckningen. Det samma gäller din webbplats: du kan inte använda en
  officiell *.debian.org-domän så som beskrivs i
  <a href="$(HOME)/devel/dmup">DMUP</a> (Debian Machine Usage Policy).
 </li>
</ul>

<p>
Om något av kriterierna ovan vid någon punkt i framtiden inte längre
uppfylls får konsulten en varning om att de är på väg att tas bort ur
förteckningen såvida de inte åter uppfyller reglerna.
Konsulten får fyra veckor på sig.
</p>

<p>
Vissa (eller alla) delar av konsultens information kan tas bort om de inte
längre följer denna policy, eller om ansvariga så önskar, eller om
de ansvariga för förteckningen anser att så skall göras.
</p>

<h2>Tillägg, ändringar och borttagning av poster</h2>

<p>
Om du vill att ditt företag skall läggas till på denna sida, skicka
e-post till <a href="mailto:consultants@debian.org">consultants@debian.org</a> 
(på engelska) med tillämpliga delar av följande information som du
vill ska listas (e-postadress krävs, resten är valfritt enligt egna önskemål):
</p>

<ul>
  <li>Land som du vill listas under</li>
  <li>Namn</li>
  <li>Företag</li>
  <li>Adress</li>
  <li>Telefonnummer</li>
  <li>Telefaxnummer</li>
  <li>E-post</li>
  <li>Webbadress</li>
  <li>Taxa</li>
  <li>Ytterligare information</li>
</ul>

<p>
Om du vill uppdatera informationen skickar du e-post till
<a href="mailto:consultants@debian.org">consultants@debian.org</a> (på engelska),
helst från den e-postadress som nämns på konsultsidan
<a href="https://www.debian.org/consultants/">konsultsidan</a>.
</p>

# translators can, but don't have to, translate the rest - yet
# BEGIN future version
# fill out the following submission form:</p>
# 
# <form method=post action="https://cgi.debian.org/cgi-bin/submit_consultant.pl">
# 
# <p>
# <input type="radio" name="submissiontype" value="new" checked>
# Nyanmälning av konsult
# <br />
# <input type="radio" name="submissiontype" value="update">
# Uppdatera en befintlig konsult
# <br />
# <input type="radio" name="submissiontype" value="remove">
# Ta bort en befintlig konsult
# </p>
# 
# <p>Namn:<br />
# <input type="text" name="name" size="50"></p>
# 
# <p>Företag:<br />
# <input type="text" name="company" size="50"></p>
# 
# <p>Adress:<br />
# <textarea name="comment" cols=35 rows=4></textarea></p>
# 
# <p>Telefon:<br />
# <input type="text" name="phone" size="50"></p>
# 
# <p>Telefax:<br />
# <input type="text" name="fax" size="50"></p>
# 
# <p>E-post:<br />
# <input type="text" name="email" size="50"></p>
# 
# <p>Webbadress:<br />
# <input type="text" name="url" size="50"></p>
# 
# <p>Taxa:<br />
# <textarea name="comment" cols=35 rows=4></textarea></p>
# 
# <p>Ytterligare information, om sådan finns (<em>på engelska</em>):<br />
# <textarea name="comment" cols=40 rows=7></textarea></p>
# 
# </form>
# 
# <p>
# Om du av någon orsak inte kan sända in ovanstående, ber vi dig sända
# informationen per e-post <em>på engelska</em> till
# END future version
