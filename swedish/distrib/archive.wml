#use wml::debian::template title="Arkiverade utgåvor"
#use wml::debian::translation-check translation="b4319ab75a5556391ce61af8f3457785e5efdbe7"
#use wml::debian::toc

<toc-display />

<toc-add-entry name="old-archive">Debianarkivet (debian-archive)</toc-add-entry>

<p>Om du har behov av att komma åt en av de äldre Debianutgåvorna
finns de i <a href="https://archive.debian.org/debian/">Debians arkiv</a>,
<tt>https://archive.debian.org/debian/</tt>.</p>

<p>Utgåvorna sparas under sina kodnamn i dists-katalogen.</p>
<ul>
  <li><a href="../releases/stretch/">stretch</a> är Debian 9</li>
  <li><a href="../releases/jessie/">jessie</a> är Debian 8.0</li>
  <li><a href="../releases/wheezy/">wheezy</a> är Debian 7.0</li>
  <li><a href="../releases/squeeze/">Squeeze</a> är Debian 6.0</li>
  <li><a href="../releases/lenny/">Lenny</a> är Debian 5.0</li>
  <li><a href="../releases/etch/">Etch</a> är Debian 4.0</li>
  <li><a href="../releases/sarge/">Sarge</a> är Debian 3.1</li>
  <li><a href="../releases/woody/">Woody</a> är Debian 3.0</li>
  <li><a href="../releases/potato/">Potato</a> är Debian 2.2</li>
  <li><a href="../releases/slink/">Slink</a> är Debian 2.1</li>
  <li><a href="../releases/hamm/">Hamm</a> är Debian 2.0</li>
  <li>Bo   är Debian 1.3</li>
  <li>Rex  är Debian 1.2</li>
  <li>Buzz är Debian 1.1</li>
</ul>

<p>Vi har endast källkod för utgåvor som är äldre än bo, och binärer och
källkod för bo och nyare utgåvor. Allt eftersom tiden går kommer vi att
ta bort de binära paketen för gamla utgåvor.</p>

<p>Om du använder APT använder du dessa sources.list-poster:
</p>
<pre>
  deb http://archive.debian.org/debian/ hamm contrib main non-free
</pre>
<p>
eller
</p>
<pre>
  deb http://archive.debian.org/debian/ bo bo-unstable contrib main non-free
</pre>

<p>rsync-åtkomst finns tillgänglig via <pre>rsync.archive.debian.org</pre></p>

<p>Här följer en lista över speglar som innehåller arkivet:</p>

#include "$(ENGLISHDIR)/distrib/archive.mirrors"
<archivemirrors>

<toc-add-entry name="non-us-archive">Debians icke-USA-arkiv (debian-non-US)</toc-add-entry>

<p>
Det fanns tidigare programvara som paketerats för Debian som inte kunde
distribueras i USA (och andra länder) på grund av restriktioner på export av
kryptografi eller programvarupatent.
Debian hade ett speciellt arkiv kallat <q>non-US</q>-arkivet.
</p>

<p>
Dessa paket togs in i huvudarkivet i Debian 3.1 och arkivet debian-non-US
används inte längre; numera är det <em>arkiverat</em> och medtaget i
arkiven på archive.debian.org.
</p>

<p>
De är fortfarande tillgängliga från maskinen archive.debian.org:
Tillgängliga åtkomstmetoder är:
</p>

<blockquote><p>
<a href="https://archive.debian.org/debian-non-US/">http://archive.debian.org/debian-non-US/</a><br>
rsync://rsync.archive.debian.org/debian-non-US/
</p></blockquote>

<p>
För att använda dessa paket med APT använder du poster motsvarande dessa
i sources.list:
</p>

<pre>
  deb http://archive.debian.org/debian-non-US woody/non-US main contrib non-free
  deb-src http://archive.debian.org/debian-non-US woody/non-US main contrib non-free
</pre>
