#!/usr/bin/python3

# This script is still used in 2024

# get-www-stats - Debian web site popularity statistics
# Copyright 2010 Marcin Owsiany <porridge@debian.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# This program is run from debwww crontab on Debian website master server.

try:
  import json
except ImportError:
  import simplejson as json

from collections import defaultdict
import logging
import re
import subprocess
import sys

counts = defaultdict(int)

# Use subprocess to execute the shell command with globbing
command = 'zcat /srv/weblogs.debian.org/incoming/*.debian.org/www.debian.org-access.log*'

# Create a subprocess to run the shell command
process = subprocess.Popen(
    command, shell=True, stdout=subprocess.PIPE,
    stderr=subprocess.PIPE, text=True
)

with process.stdout:
    for line in process.stdout:
      tokens = line.split(maxsplit=9)
      if tokens[8] != '200':
        continue
      if tokens[5] != '"GET':
        continue
      url = tokens[6]
      url = re.sub(r'#.*$', '', url)
      url = re.sub(r'\?.*$', '', url)
      url = re.sub(r'//+', '/', url)
      url = re.sub(r'/(\./)+', '/', url)
      url = re.sub(r'^/\.\./', '/', url)
      url = re.sub(r'/[^./]*/\.\./', '/', url)
      url = re.sub(r'\.([a-z]{2}|[a-z]{2}-[a-z]{2})\.(html|xml|rdf|pdf)$', '', url)
      url = re.sub(r'\.(html|xml|rdf|pdf)(\.([a-z]{2}|[a-z]{2}-[a-z]{2}))?$', '', url)
      url = re.sub(r'/$', '/index', url)
      counts[url] += 1

if '/index' not in counts:
  raise Exception('No data for /index')
elif counts['/index'] < 50000:
  logging.warn('Less than 50k hits for /index')
elif counts['/index'] < 10000:
  raise Exception('Less than 10k hits for /index')


# Check for errors
stderr = process.stderr.read()
if stderr:
    print(f"Error: {stderr}", file=sys.stderr)

# wait fetches the return code of the process
return_code = process.wait()
if return_code != 0:
    print(f"Command exited with return code {return_code}", file=sys.stderr)

json.dump(sorted([(v, k) for (k, v) in counts.items() if v > 2], reverse=True),
          sys.stdout,
          indent=2)
