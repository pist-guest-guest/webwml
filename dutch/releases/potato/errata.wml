#use wml::debian::template title="Debian GNU/Linux 2.2 -- Errata" BARETITLE=true
#use wml::debian::translation-check translation="534d1b782cfb92f46dc41fd064f779fffc329b12"

<p><strong>Debian GNU/Linux 2.2 werd vervangen door
<a href="../woody/">Debian GNU/Linux 3.0 ("woody")</a>.
Er worden geen beveiligingsupdates meer uitgebracht sinds 30 juni 2003.</strong>
Raadpleeg
<a href="https://lists.debian.org/debian-devel-announce/2003/debian-devel-announce-200302/msg00010.html">\
de resultaten van de enquête van het beveiligingsteam</a> voor meer informatie.</p>

<h2>Veiligheidsproblemen</h2>

<p>Het Debian veiligheidsteam brengt updates uit voor pakketten uit de stabiele
release waarin problemen in verband met beveiliging vastgesteld werden.
Raadpleeg de <a href="$(HOME)/security/">beveiligingspagina's</a> voor informatie
over eventuele beveiligingsproblemen die in `potato' ontdekt werden.</p>

<p>Als u APT gebruikt, voeg dan de volgende regel toe aan <tt>/etc/apt/sources.list</tt>
om toegang te hebben tot de laatste beveiligingsupdates:</p>

<pre>
  deb http://security.debian.org/ potato/updates main contrib non-free
</pre>

<p>Voer daarna <kbd>apt-get update</kbd> uit, gevolgd door
<kbd>apt-get upgrade</kbd>.</p>

<h2>Tussenreleases</h2>

<p>Soms, in het geval van diverse kritieke problemen of beveiligingsupdates,
wordt de uitgebrachte distributie bijgewerkt. Een
dergelijke bijwerking wordt gewoonlijk aangeduid als een tussenrelease.</p>

<ul>
  <li>De eerste tussenrelease, 2.2r1, werd uitgebracht op <a href="$(HOME)/News/2000/20001114">14 november 2000</a>.</li>
  <li>De tweede tussenrelease, 2.2r2, werd uitgebracht op <a href="$(HOME)/News/2000/20001205">3 december 2000</a>.</li>
  <li>De derde tussenrelease, 2.2r3, werd uitgebracht op <a href="$(HOME)/News/2001/20010417">17 april 2001</a>.</li>
  <li>De vierde tussenrelease, 2.2r4, werd uitgebracht op <a href="$(HOME)/News/2001/20011105">5 november 2001</a>.</li>
  <li>De vijfde tussenrelease, 2.2r5, werd uitgebracht op <a href="$(HOME)/News/2002/20020110">10 januari 2002</a>.</li>
  <li>De zesde tussenrelease, 2.2r6, werd uitgebracht op <a href="$(HOME)/News/2002/20020403">3 april 2002</a>.</li>
  <li>De zevende tussenrelease, 2.2r7, werd uitgebracht op <a href="$(HOME)/News/2002/20020713">13 juli 2002</a>.</li>
</ul>

<p>
Zie de <a href="http://archive.debian.org/debian/dists/potato/ChangeLog">
ChangeLog</a> (en de
<a href="http://archive.debian.org/debian-non-US/dists/potato/non-US/ChangeLog">
ChangeLog voor non-US</a>) voor details over wijzigingen.</p>

<p>Verbeteringen voor de uitgebrachte stabiele distributie gaan dikwijls door een
uitgebreide testperiode voordat ze in het archief worden aanvaard.
Deze verbeteringen zijn echter wel beschikbaar in de map
<a href="http://archive.debian.org/debian/dists/potato-proposed-updates/">
dists/potato-proposed-updates</a>  van elke Debian archief-spiegelserver
(en op dezelfde locatie op onze
<a href="http://archive.debian.org/debian-non-US/dists/potato-proposed-updates/">
non-US-server</a> en zijn spiegelservers).</p>

<p>Als u <tt>apt</tt> gebruikt om uw pakketten bij te werken, dan kunt u de
voorgestelde updates installeren door de volgende regel toe te voegen aan
<tt>/etc/apt/sources.list</tt>:</p>

# In vertaalde versies zouden deze links wellicht
# waar mogelijk aangepast moeten worden.
<pre>
  \# voorgestelde toevoegingen aan een tussenrelease van 2.2
  deb http://archive.debian.org dists/potato-proposed-updates/
  deb http://archive.debian.org/debian-non-US dists/potato-proposed-updates/
</pre>

<p>Voer daarna <kbd>apt-get update</kbd> uit, gevolgd door
<kbd>apt-get upgrade</kbd>.</p>

# iemand zou moeten controleren of dit nog altijd geldt voor 2.2r3:
#
# <h2>cd-images bouwen</h2>
#
# <p>Mensen die aangepaste cd-images willen maken voor 2.2r2, zullen de
# CVS-versie van `debian-cd' moeten gebruiken en <strong>niet</strong>
# de versie die momenteel in potato geïnstalleerd is.</p>

<h2>Niet-officiële geïnternationaliseerde installatieondersteuning</h2>

<p>Er is een geïnternationaliseerde versie van het installatiesysteem voor i386 beschikbaar (https://people.debian.org/~aph/current-i18n/) om te testen en te gebruiken. U moet de variant 'idepci' of 'compact' gebruiken.</p>

<p>Vanwege enkele resterende problemen met het geïnternationaliseerde installatiesysteem is het onwaarschijnlijk dat het officieel zal worden opgenomen in een tussenrelease van Potato. We kunnen alleen maar hopen dat we het voor <a href="../woody/">woody</a> in orde kunnen krijgen.</p>


<h2>Niet-officiële ondersteuning voor ReiserFS</h2>

<p>Er lijken minstens twee niet-officiële versies van het installatiesysteem
voor i386 te zijn die ReiserFS ondersteunen</p>

<p>Een versie is van John H. Robinson, IV, en is beschikbaar met
enkele instructies op <a
href="http://chao.ucsd.edu/debian/boot-floppies/">
http://chao.ucsd.edu/debian/boot-floppies/</a>.</p>

<p>De andere is van Marc A. Volovic en is beschikbaar op
http://www.bard.org.il/~marc/linux/rfs/.</p>


<h2>Niet-officiële ondersteuning voor Linux Kernel 2.4</h2>

<p>
Adrian Bunk heeft gezorgd voor een reeks pakketten i386-systemen met Potato die
compatibiliteit bieden met Linux Kernel 2.4.x. Lees <a
href="http://www.fs.tum.de/~bunk/kernel-24.html">
http://www.fs.tum.de/~bunk/kernel-24.html</a> voor instructies,
waarschuwingen en aan <code>/etc/apt/sources.list</code>
toe te voegen regels.</p>
