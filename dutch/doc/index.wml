#use wml::debian::template title="Documentatie" MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="019351f156c8f8ffc0f7922929f7062fb4b732db"

# Last translation update by: $Author$
# Last translation update at: $Date$

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#quick_start">Snel aan de slag</a></li>
  <li><a href="#manuals">Handleidingen</a></li>
  <li><a href="#other">Andere (kortere) documenten</a></li>
</ul>

<p>
Bij het maken van een vrij besturingssysteem van hoge kwaliteit hoort ook het schrijven van technische handleidingen die de werking en het gebruik van programma's beschrijven. Het Debian-project stelt alles in het werk om alle gebruikers te voorzien van goede documentatie in een gemakkelijk toegankelijke vorm. Deze pagina bevat een aantal links die leiden naar installatiehandleidingen, HOWTO's, veelgestelde vragen, opmerkingen bij de release, onze Wiki en meer.
</p>

<h2><a id="quick_start">Snel aan de slag</a></h2>


<p>
Als Debian nieuw voor u is, raden we u aan te beginnen met de volgende twee handleidingen:
</p>

<aside class="light">
  <span class="fas fa-fast-forward fa-5x"></span>
</aside>

<ul>
  <li><a href="$(HOME)/releases/stable/installmanual">Installatiehandleiding</a></li>
  <li><a href="manuals/debian-faq/">Debian GNU/Linux FAQ</a></li>
</ul>

<p>
Zorg ervoor dat u deze documenten bij de hand heeft wanneer u uw eerste
installatie van Debian uitvoert. Waarschijnlijk zullen ze veel vragen
beantwoorden en u helpen bij het werken met uw nieuw Debian-systeem.
</p>

<p>
Later zult u mogelijk de volgende documenten willen raadplegen:
</p>

<ul>
  <li><a href="manuals/debian-reference/">Debian Reference</a> (Debian Naslagwerk): een beknopte gebruikershandleiding met de klemtoon op de shell-commandoregel (nog niet naar het Nederlands vertaald)</li>
  <li><a href="$(HOME)/releases/stable/releasenotes">Notities bij de release</a>: meestal gepubliceerd bij updates van Debian, gericht op gebruikers die een opwaardering van de distributie uitvoeren</li>
  <li><a href="https://wiki.debian.org/">Debian Wiki</a>: officiële Debian-wiki (beperkt aantal pagina's in het Nederlands)</li>
</ul>

<p style="text-align:center"><button type="button"><span class="fas fa-print fa-2x"></span> <a href="https://www.debian.org/doc/manuals/refcard/refcard">De Debian GNU/Linux Naslagkaart afdrukken</a></button></p>

<h2><a id="manuals">Handleidingen</a></h2>

<p>
Het grootste deel van de documentatie die is opgenomen in Debian, werd geschreven voor GNU/Linux in het algemeen, maar er is ook documentatie die specifiek voor Debian is geschreven. In principe worden de documenten ingedeeld in de volgende categorieën:
</p>

<ul>
  <li>Handleidingen: deze gidsen lijken op boeken, omdat ze belangrijke onderwerpen uitgebreid beschrijven. Veel van de hieronder vermelde handleidingen zijn zowel online als in Debian-pakket beschikbaar. In feite zijn de meeste handleidingen op de website uit hun respectievelijk Debian-pakket gehaald. Kies hieronder een handleiding voor de pakketnaam en/of de link naar de online versie.</li>
  <li>HOWTO's: zoals de naam aangeeft, beschrijven de <a href="https://tldp.org/HOWTO/HOWTO-INDEX/categories.html">HOWTO-documenten</a> <em>hoe</em> u een bepaalde zaak moet <em>doen</em>, d.w.z. zij geven gedetailleerd en praktisch advies over de manier waarop iets moet worden gedaan.</li>
  <li>FAQ's: we hebben verschillende documenten samengesteld waarin <em>veelgestelde vragen</em> worden beantwoord. Debian-gerelateerde vragen worden beantwoord in de <a href="manuals/debian-faq/">Debian FAQ</a>. Daarnaast is er een aparte <a href="../CD/faq/">FAQ over Debian USB/cd/dvd-images</a>, waarin allerlei vragen over installatiemedia worden beantwoord.</li>
  <li>Andere (kortere) documenten: bekijk ook de <a href="#other">lijst</a> met beknopte instructies.</li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Raadpleeg de pagina van het <a href="ddp">Documentatieproject van Debian</a> voor een volledige lijst van Debian-handleidingen en andere documentatie. Bovendien zijn er verschillende gebruikersgerichte handleidingen voor Debian GNU/Linux beschikbaar als <a href="books">gedrukte boeken</a>.</p>
</aside>

<p>Veel van de hier vermelde handleidingen zijn zowel online als in Debian-pakket beschikbaar; in feite zijn de meeste handleidingen op de website uit hun respectievelijk Debian-pakket gehaald. Kies hieronder een handleiding voor de naam van het pakket en/of de link naar de online versie.</p>

<h3>Handleidingen die specifiek zijn voor Debian</h3>

<div class="line">
  <div class="item col50">

    <h4><a href="user-manuals">Handleidingen voor gebruikers</a></h4>
    <ul>
      <li><a href="https://debian-beginners-handbook.tuxfamily.org/index-en.html">The Debian Bookworm beginner’s handbook (Debian Bookworm handboek voor beginners - nog niet in het Nederlands)</a></li>
      <li><a href="user-manuals#faq">Debian GNU/Linux FAQ</a> (veelgestelde
vragen)</li>
      <li><a href="user-manuals#install">Debian Installatiehandleiding</a></li>
      <li><a href="user-manuals#relnotes">De Debian Notities bij de Release</a></li>
      <li><a href="user-manuals#refcard">Debian Naslagkaart</a></li>
      <li><a href="user-manuals#debian-handbook">Het Debian Beheerdershandboek</a></li>
      <li><a href="user-manuals#quick-reference">Debian Reference</a> (Debian
naslagwerk - nog niet in het Nederlands)</li>
      <li><a href="user-manuals#securing">Securing Debian Manual</a>
(Handleiding voor het beveiligen van Debian - nog niet in het
Nederlands)</li>
      <li><a href="user-manuals#aptitude">Gebruikershandleiding voor
aptitude</a></li>
      <li><a href="user-manuals#apt-guide">APT User's Guide</a> (Gebruikershandleiding voor APT - nog niet in het Nederlands)</li>
      <li><a href="user-manuals#java-faq">Debian GNU/Linux and Java FAQ</a> (De
FAQ over Debian GNU/Linux en Java - nog niet in het Nederlands)</li>
      <li><a href="user-manuals#hamradio-maintguide">Debian Hamradio Maintainer’s Guide</a> (Debian handleiding voor onderhouders van amateurradio-pakketten)</li>
    </ul>

  </div>

  <div class="item col50 lastcol">

    <h4><a href="devel-manuals">Handleidingen voor ontwikkelaars</a></h4>
    <ul>
      <li><a href="devel-manuals#policy">Debian Policy Manual</a> (Het Debian
Beleidshandboek - nog niet in het Nederlands)</li>
      <li><a href="devel-manuals#devref">Debian Developer's Reference</a> (Het
naslagwerk voor ontwikkelaars van Debian - nog niet in het Nederlands)</li>
      <li><a href="devel-manuals#debmake-doc">Guide for Debian
Maintainers</a> (Handleiding voor pakketbeheerders van Debian - nog niet in
het Nederlands)</li>
      <li><a href="devel-manuals#packaging-tutorial">Introduction to Debian
packaging</a> (Inleiding in het maken van Debian-pakketten - nog niet in het
Nederlands)</li>
      <li><a href="devel-manuals#menu">Debian Menu System</a> (Uitleg over het
Debian menusysteem - nog niet in het Nederlands)</li>
      <li><a href="devel-manuals#d-i-internals">Debian Installer
internals</a> (Achter de schermen van het Debian installatiesysteem - nog
niet in het Nederlands)</li>
      <li><a href="devel-manuals#dbconfig-common">Guide for database using package maintainers</a> (Handleiding voor
beheerders van pakketten die databases gebruiken - nog niet in het Nederlands) </li>
      <li><a href="devel-manuals#dbapp-policy">Policy for packages using databases</a> (Richtlijnen voor
pakketten die databases gebruiken - nog niet in het Nederlands) </li>
    </ul>

  </div>
</div>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="misc-manuals#history">De geschiedenis van het Debian-project lezen</a></button></p>

<h2><a id="other">Andere (beknoptere) documenten</a></h2>

<p>De volgende documenten bevatten snelle, korte instructies:</p>

<p>
<dl>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Als u de bovenstaande bronnen hebt geraadpleegd en nog steeds geen antwoorden op uw vragen of oplossingen voor uw problemen met betrekking tot Debian kunt vinden, neem dan een kijkje op onze <a href="../support">ondersteuningspagina</a>.</p>
</aside>

  <dt><strong>Man-pagina’s</strong></dt>
    <dd>Vanouds worden alle UNIX programma's gedocumenteerd in
        zogenaamde <q><em>manpages</em></q> (man-pagina’s). Dit zijn
        naslaghandleidingen die u kunt oproepen door middel van het commando
        <tt>man</tt>. Ze zijn meestal niet gericht op beginners, maar ze
        documenteren alle eigenschappen en functies van een commando. Het
        volledige archief van alle man-pagina’s die in Debian beschikbaar zijn,
        staat online:
        <a href="https://manpages.debian.org/cgi-bin/man.cgi">https://manpages.debian.org/</a>.
    </dd>

  <dt><strong><a href="https://www.gnu.org/software/texinfo/manual/texinfo/html_node/index.html">info-bestanden</a></strong></dt>
    <dd>Veel software van GNU is gedocumenteerd in <em>info-bestanden</em> in
    plaats van in man-pagina’s. Deze bestanden bevatten uitgebreide informatie
    over het programma zelf, opties en voorbeelden. Ze zijn beschikbaar via het
    commando <tt>info</tt>.
    </dd>

  <dt><strong>README-bestanden</strong></dt>
    <dd>README-bestanden (lees-mij-bestanden) zijn eenvoudige tekstbestanden die
	één enkel onderwerp beschrijven, meestal een specifiek pakket. U
	kunt veel van deze bestanden vinden in de onderliggende mappen van
	<tt>/usr/share/doc/</tt> op uw Debian-systeem.
	Naast het README-bestand bevatten sommige van die mappen
    configuratievoorbeelden. Houd er rekening mee dat de documentatie van
    grotere programma's doorgaans in een apart pakket wordt geleverd
	(met dezelfde naam als het oorspronkelijke pakket, maar eindigend op
	<em>-doc</em>).
    </dd>

 </dl>
</p>
