#use wml::debian::cdimage title="Netwerkinstallatie vanaf een minimale USB, cd"
#use wml::debian::release_info
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="678a572e92767d98d57f67822e00905782bf9637"

# Last Translation Update by $Author$
# Last Translation Update at $Date$

<div class="tip">
<p>Op de architecturen i386 en amd64, kunnen alle USB/cd/dvd-images ook
<a href="https://www.debian.org/CD/faq/#write-usb">op een USB-stick gebruikt worden</a>.</div>

<p>Een <q>netwerkinstallatie</q>- of <q>netinst</q>-cd is één cd die u in staat
stelt om het volledige besturingssysteem te installeren. Deze ene cd bevat
enkel de minimale hoeveelheid aan software om het basissysteem te installeren
en de overige pakketten op te halen via het internet.</p>

<p><strong>Welke types van netwerkverbindingen worden ondersteund tijdens de
installatie?</strong>
De netwerkinstallatie gaat ervan uit dat u een verbinding met internet heeft.
Hierbij worden verschillende manieren ondersteund, zoals ethernet en WLAN (met enkele beperkingen)</p>

<p>De volgende minimale opstartbare cd-images kunnen gedownload worden:</p>

<ul>

<li>Officiële "netinst" images voor de "stable" release &mdash;
<a href="#netinst-stable">zie hieronder</a></li>

<li>Images voor de release <q>testing</q> &mdash; zie de
<a href="$(DEVEL)/debian-installer/">Debian-Installer pagina</a>.</li>

</ul>

<h2 id="netinst-stable">Officiële netinstallatie-images voor de
<q>stable</q> release</h2>

<p>Dit image bevat het installatiesysteem en een klein
aantal pakketten dat de installatie van een (erg) basaal systeem toelaat.</p>

<div class="line">
<div class="item col50">
<p><strong>
netinst cd-image
</strong></p>
  <stable-netinst-images />
</div>
<div class="item col50 lastcol">
<p><strong>
netinst cd-image (via <a href="$(HOME)/CD/torrent-cd">bittorrent</a>)
</strong></p>
  <stable-netinst-torrent />
</div>
<div class="clear"></div>
</div>

<p>Voor informatie over deze bestanden en hoe ze te gebruiken, zie de
<a href="../faq/">FAQ</a>.</p>

<p>Vergeet na het downloaden van de images niet om ook de
<a href="$(HOME)/releases/stable/installmanual">gedetailleerde informatie over
het installatieproces</a> te bekijken.</p>

