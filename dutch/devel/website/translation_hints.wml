#use wml::debian::template title="Nuttige vertaalsuggesties"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="d49333f670c2639f601fd741d3dd5f060de97e2b"

<p>Zie de pagina's over <a href="working">werken aan de webpagina's</a> waarin
enkele algemene zaken worden beschreven die in acht moeten worden genomen, niet
strikt beperkt tot vertalingen.

<h2>Wat vertalen?</h2>

<p>Zie de <a href="translating#completenew">instructies over het beginnen van
een nieuwe vertaling</a> voor een inleiding.</p>

<p>Wanneer u begint met het vertalen van pagina's, raden we u aan te beginnen
met pagina's die hoogstwaarschijnlijk bezocht zullen worden door bezoekers.
Hierna volgen enkele richtlijnen. Houd er ook rekening mee dat de lijsten met
pagina's in de <a href="stats/">vertaalstatistieken</a> gerangschikt worden
volgens populariteit.</p>

<dl>
<dt><strong>Belangrijkst:</strong></dt>
  <dd>
  <ul>
    <li>de hoofdmap: index.wml, contact.wml, donations.wml,
        social_contract.wml, support.wml</li>
    <li>de map intro/: about.wml, cn.wml, free.wml, why_debian.wml</li>
    <li>de map releases/: index.wml</li>
    <li>de map releases/<current_release_name>/: index.wml,
        installmanual.wml, releasenotes.wml</li>
    <li>de map distrib/: index.wml, packages.wml, netinst.wml, ftplist.wml</li>
    <li>de map mirror/: list.wml</li>
    <li>de map CD/: index.wml</li>
    <li>de map doc/: index.wml</li>
    <li>de map MailingLists/: index.wml</li>
    <li>de map security/: index.wml</li>
  </ul>
  </dd>
<dt><strong>Standaard:</strong></dt>
  <dd>De overige bestanden in de bovengenoemde mappen en wat hierna volgt:
  <ul>
    <li>Bugs/index.wml, Bugs/Reporting.wml</li>
    <li>banners/index.wml</li>
    <li>blends/index.wml</li>
    <li>consultants/index.wml</li>
    <li>doc/ddp.wml</li>
    <li>events/index.wml</li>
    <li>international/index.wml, en maak een pagina (of map) voor uw
        taal</li>
    <li>logos/index.wml</li>
    <li>mirror/index.wml</li>
    <li>misc/index.wml</li>
    <li>News/index.wml</li>
    <li>News/weekly/index.wml</li>
    <li>ports/index.wml</li>
    <li>partners/index.wml</li>
  </ul>
  </dd>
<dt><strong>Facultatief:</strong></dt>
  <dd>Alle andere bestanden in de eerder genoemde mappen.
  Dit omvat ook de volgende mappen die onderliggende mappen bevatten die
  regelmatig worden gewijzigd en dus moeilijker up-to-date te houden zijn:
  <ul>
    <li>MailingLists/desc/</li>
    <li>News/</li>
    <li>doc/books.wml</li>
    <li>events/</li>
    <li>security/</li>
  </ul>
  </dd>
<dt><strong>Minst belangrijk:</strong></dt>
  <dd>Bestanden in de mappen devel/ en vote/. Omdat deze meestal voor
  ontwikkelaars bedoeld zijn, en de primaire taal van ontwikkelaars Engels
  is, moet u hun vertaling pas aanpakken wanneer u over een sterk
  vertaalteam beschikt.</dd>
</dl>

<p>
<strong>Het is belangrijk dat u alleen bestanden vertaalt waarvoor u de tijd
heeft om ze te onderhouden. Een paar goed onderhouden pagina's is veel nuttiger
dan een heleboel verouderde.</strong>

<h2>Hoe nauwkeurig moeten vertalingen het origineel volgen?</h2>

<p>Er zijn momenten waarop u misschien een verandering wilt aanbrengen in de
inhoud wanneer u aan het vertalen bent. Een voorbeeld is de
ondersteuningspagina. U zult waarschijnlijk een voorbeeld willen toevoegen over
het inschrijven op de taalspecifieke mailinglijst, bijv. op debian-user-dutch in
de Nederlandse versie van de pagina's.

<p>Als u meer ingrijpende wijzigingen aanbrengt, meld dit dan op
<a href="mailto:debian-www@lists.debian.org">debian-www list</a>,
aangezien het wenselijk is om de inhoud tussen de verschillende versies zo
gelijk mogelijk te houden.

<p>Het is de bedoeling dat de pagina's nuttig zijn. Als u informatie heeft die
de gebruikers van uw taal kan helpen, kunt u deze toevoegen. U kunt
international/&lt;Taal&gt;.wml gebruiken voor alle dingen die interessant zijn
voor <em>Taal</em>-talige bezoekers.

<p>Als u weet heeft van informatie die nuttig zou kunnen zijn voor alle
gebruikers, breng dit dan ter sprake op debian-www.

<h2>Hoe weten vertalers wanneer bestanden moeten worden bijgewerkt?</h2>

<P>Er is een mechanisme dat vertalers kunnen gebruiken om
<a href="uptodate">vertalingen van websites up-to-date te houden</a>.

<h2>Hoe houden we de vertalingen van gettext-sjablonen up-to-date?</h2>

<p>Nadat de Engelse bestanden bijgewerkt werden, moet u de opdracht
<kbd>make update-po</kbd> uitvoeren in de onderliggende map <code>po/</code>
van uw vertaling om uw .po-bestanden bij te werken met de originele.
Het bekijken van de logberichten op de
<a href="https://lists.debian.org/debian-www-cvs/">mailinglijst debian-www-cvs</a>
kan nuttig zijn om erachter te komen wanneer dit moet gebeuren; of u kunt het
gewoon met regelmatige tussenpozen uitvoeren.</p>

<p>Gebruik het commando <kbd>make stats</kbd> om een overzicht van de
veranderingen te zien. Gettext zal de tags waarvan het de waarde moest raden,
markeren met "<code>#, fuzzy</code>", en nieuw ingevoerde tags zullen gewoon
een lege tekenreeks krijgen na <code>msgstr</code>.</p>

<h2>Hoe weten gebruikers of een vertaalde pagina verouderd is?</h2>

<P>Het sjabloon <code>translation-check</code> dat gebruikt wordt om
<a href="uptodate">vertalingen up-to-date te houden</a> zal een notitie
toevoegen aan vertalingen die verouderd zijn.

<h2>Waar u op moet letten bij het vertalen</h2>

<p>Hieronder volgt een lijst van pagina's en mappen die bij het vertalen mogelijk speciale aandacht vereisen:

<dl>
<dt><tt>News/</tt>
   <dd>U kunt zo veel of zo weinig nieuwsstukken vertalen als u wilt. De
   indexen worden automatisch aangemaakt op basis van de titels van de items.
   Als een item vertaald is, dan zal de vertaalde titel in de index gebruikt
   worden.</dd>

<dt><tt>security/</tt>
   <dd>Dit is op dezelfde manier opgezet als de map News/. Er is één verschil,
   er zijn .data-bestanden die u <em>niet</em> moet vertalen.</dd>

<dt><tt>CD/vendors/</tt>
   <dd>Enkel de *.wml-bestanden in CD/vendors/ moeten vertaald worden.
   Vertalingen voor tags worden via gettext toegevoegd in het bestand
   po/vendors.<var>xy</var>.po.</dd>

<dt><tt>intro/organization.wml</tt>
   <dd>Tags worden vertaald via gettext in het bestand
   po/organization.<var>xy</var>.po.</dd>

<dt><tt>MailingLists/{un,}subscribe.wml</tt>
   <dd>Deze beide bestanden worden gegenereerd door het script <tt>mklist</tt>.
   U kunt ze dus niet rechtstreeks bewerken. U kunt de bestanden vertalen in de
   onderliggende map desc/. Deze bevatten de beschrijvingen van de
   mailinglijsten. Tags worden via gettext vertaald in het bestand
   po/mailinglists.<var>xy</var>.po.
   </dd>

<dt><tt>consultants/index.wml</tt>
   <dd>Tags worden vertaald via gettext in het bestand
   po/consultants.<var>xy</var>.po.</dd>

<dt><tt>releases/*/{installmanual,releasenotes}.wml</tt>
   <dd>Alles vertalen, behalve de Perl-code (zaken tussen &lt;: :&gt;),
   behalve het <strong>tweede</strong> argument van permute_as_list.</dd>

<dt><tt>ports/</tt>
   <dd>Pagina's over het overdragen van Debian naar bepaalde architecturen
   kunnen vluchtig zijn. U zou deze enkel moeten vertalen als u bereid bent om
   tijd te investeren in het up-to-date houden van deze pagina's.</dd>

<dt><tt>devel/website/</tt>
   <dd>Dit is bedoeld voor mensen die webpagina's bewerken of vertalen, en dus
   heeft dit waarschijnlijk een erg lage prioriteit.</dd>
</dl>
