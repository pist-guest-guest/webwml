#use wml::debian::template title="Debian installeren via het internet" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="75e33068da4f64cce68c5fda8f15b7d6cd7179ea"

#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"

# Translator: $Author$
# Last update: $Date$

<p>Deze methode om Debian te installeren vereist een werkende
internetverbinding <em>tijdens</em> de installatie. Vergeleken met andere
methodes zult u minder data downloaden omdat het proces wordt aangepast aan uw
behoeften. Ethernet en draadloze verbindingen worden ondersteund.</p>

<div class="line">
<div class="item col50">
<h2>Kleine cd's of USB-sticks</h2>

<p>Hierna vindt u imagebestanden. Raadpleeg <a href="../CD/netinst/">Netwerkinstallatie vanaf een minimale cd</a> voor nadere informatie.</p>

<stable-netinst-images />
</div>
<div class="clear"></div>
</div>

<div class="line">
<div class="item col50">

<br/>
<h2>Mini-cd's, flexibele USB-sticks, enz.</h2>

<p><strong>Voor gevorderde gebruikers</strong>: U kunt een aantal kleine imagebestanden, geschikt voor USB-sticks en
vergelijkbare media, downloaden, deze daarna naar het medium wegschrijven
en vervolgens de installatie starten door uw systeem vanaf dat medium op te
starten.</p>

<p>Er bestaat enige variatie tussen de architecturen in de ondersteuning
van een installatie vanaf de diverse zeer kleine images.</p>

<p>Voor meer gedetailleerde informatie verwijzen wij naar de
<a href="$(HOME)/releases/stable/installmanual">installatiehandleiding voor
uw platform</a>, met name het hoofdstuk
<q>Systeeminstallatiemedia verkrijgen</q>.</p>

<p>Hier zijn de links naar de imagebestanden (zie het bestand MANIFEST
voor informatie):</p>

<stable-verysmall-images />
</div>
<div class="item col50 lastcol">

<br/>
<h2>Netwerk-opstart</h2>

<p><strong>Voor gevorderde gebruikers</strong>: Voor deze methode richt u een TFTP- en een DHCP (of BOOTP, of RARP)-server
in waarmee de installatiemedia beschikbaar worden gemaakt voor machines op uw
lokale netwerk. Als het BIOS van uw clients dit ondersteunt, kunt u
vervolgens het Debian installatiesysteem opstarten vanaf het netwerk (met
gebruik van PXE en TFTP) en daarna Debian verder installeren, eveneens vanaf
het netwerk.</p>

<p>Merk op dat niet alle machines vanaf het netwerk kunnen worden opgestart.
Daarnaast wordt deze installatiemethode vanwege het extra werk dat ervoor
nodig is, niet aangeraden voor beginnende gebruikers.</p>

<p>Voor meer gedetailleerde informatie verwijzen wij naar de
<a href="$(HOME)/releases/stable/installmanual">installatiehandleiding voor
uw platform</a>, met name het hoofdstuk
<q>Bestanden voorbereiden voor opstarten via TFTP over het netwerk</q>.</p>

<p>Hier zijn de links naar de imagebestanden (zie het bestand MANIFEST
voor informatie):</p>

<stable-netboot-images />
</div>
</div>

