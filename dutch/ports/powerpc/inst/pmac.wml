#use wml::debian::template title="Debian voor PowerPC (PowerMac)" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/powerpc/inst/menu.inc"
#use wml::debian::translation-check translation="b5617866ae4047b55b0439a5c5796df65590cec3"

<h1 class="center">Pagina over Debian GNU/Linux PowerPC voor PowerMac</h1>

<p>
Hier volgen enkele belangrijke punten van de installatie van Debian op PowerMac.
Raadpleeg voor gedetailleerde instructies
<a href="$(HOME)/releases/stable/powerpc/">de fijne installatiehandleiding</a>.
Het team van het Debian-installatiesysteem heeft vele lange uren besteed aan
een poging om vooraf op uw vragen te antwoorden en u een uitstekende
schriftelijke begeleiding te geven bij het installeren van Debian.
</p>

<p>
Het is zeker mogelijk, en er bestaan eigenlijk vrij elegante oplossingen
voor de NewWorld Macs, om uw Debian PowerMac-systeem als dual-boot-systeem
te gebruiken samen met Mac OS en/of Mac OS X. Echter, als u van plan bent
een nieuwe installatie van Mac OS X te doen, voert u die best eerst uit voor u
Debian installeert. Het installatiesysteem van Mac OS X gedraagt zich
bij de installatie erg onvriendelijk tegenover bestaande systemen.
Binnenkort heeft u misschien ook de mogelijkheid om Debian binnen een
Darwin-systeem uit te voeren.
</p>

<p>
Het installatiesysteem van Debian gebruikt een kernel uit de 2.6-reeks.
Deze kernel zou de meeste PowerMac-hardware moeten ondersteunen.
Een powerpc-variant van de 2.4 kernel-serie is beschikbaar voor gebruikers
met extern geleverde modules die niet zijn overgezet naar 2.6-kernels.
Het gebruik van deze kernel wordt nochtans sterk afgeraden.
</p>

<p>
U zult een schijfindeling moeten uitvoeren; Linux moet op zijn eigen
partitie(s) geïnstalleerd worden. Als u een systeem met één schijf heeft,
moet u een back-up maken van alles op uw systeem en dit terugzetten nadat
de schijfindeling voltooid is. Sommige schijfindelingsprogramma's
van derden kunnen in staat zijn om een partitie te 'doen krimpen',
zodat u op uw schijf ruimte krijgt voor meerdere partities, zonder dat
wat er reeds op staat, vernield wordt, maar ongetwijfeld zullen
ook deze programma's het maken van een back-up adviseren. Drive
Setup biedt deze mogelijkheid niet, het wist de hele schijf.
</p>

<p>
1 GB is wellicht voldoende voor een experimenteel Linux-systeem.
U kunt met minder rondkomen, misschien zelfs met slechts 400&nbsp;MB
voor een heel basaal systeem, maar u zult waarschijnlijk meer willen
dan alleen het elementairste.
</p>

<p>
Na de schijfindeling zult u een installatie-cd moeten aanschaffen of
het installatiesysteem moeten downloaden. Als u uiteindelijk gaat zitten
om de installatie uit te voeren (bij voorkeur met de installatiehandleiding
bij de hand), zult u er waarschijnlijk twee tot drie uur over doen.
Een ervaren installateur krijgt een basisinstallatie klaar in minder dan
een half uur.
</p>

<p>
Indien u zich niet comfortabel voelt aan de commandoregel, moet u
na het beëindigen van de basisinstallatie de grafische werkomgeving
installeren. Maar denk er toch nog eens over na: de Linux-commandoregel
is een erg krachtig universum. Sommige zaken die in een grafische werkomgeving
erg moeilijk te realiseren zijn, kunnen erg snel en efficiënt uitgevoerd
worden aan de commandoregel. En ook, omdat het Linux-systeem gebaseerd
is op de commandoregel, zijn bepaalde functies enkel vanaf de
commandoregel toegankelijk. Bij het standaardsysteem worden
6 commandoregelconsoles ingesteld, en één grafische. U kunt veel werk
gedaan krijgen in die andere consoles terwijl u op internet surft...
en op die manier kunt u het beste van beide werelden hebben.
</p>

<p>
Het PowerPC-platform functioneert echt goed met Linux. Het wordt
zeer gerespecteerd in een groot deel van de Linux-wereld.
Geniet ervan, en vergeet niet om iets terug te geven!
</p>

<p>
Ga naar
<a href="http://penguinppc.org/bootloaders/quik/">\
http://penguinppc.org/bootloaders/quik/</a>
voor hulp met Quik op OldWorld Macs.
</p>

<p>
Voor gedetailleerde informatie over de verschillende Mac-modellen en
in het bijzonder voor informatie over het werken met OpenFirmware
bij elk van deze modellen, raadpleegt u best de
<a href="http://www.netbsd.org/Ports/macppc/models.html">NetBSD-lijst
met PowerPC-modellen</a>. Omdat bij hun installatie eerst OpenFirmware
geïnstalleerd moet worden, zijn zij de experts op dat gebied.
</p>
