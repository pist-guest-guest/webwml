<define-tag pagetitle>The Debian Project mourns the loss of Peter De Schrijver</define-tag>
<define-tag release_date>2024-07-25</define-tag>
#use wml::debian::news
# $Id$

<p>
The Debian Project mourns the sudden passing of our fellow developer and
friend, Peter De Schrijver.
</p>

<p>
Many of us knew Peter as a very helpful and dedicated person and we valued his
contributions to our project and the Linux community.
</p>

<p>
Peter was a regular and familiar face in many conferences and meets across the
world.
</p>

<p>
Peter was highly regarded for his technical expertise in problem solving and for 
his willingness to share that knowledge. When asked "what are you working on?", 
Peter would often take the time to explain something you thought was extremely 
complicated understandably, or show you in-person his high technical proficiency 
in action on such tasks as translating a disassembled binary into C source code.
</p>

<p>
Peter's work, ideals, and memory leave a remarkable legacy and a loss that is 
felt around the world not only in the many communities he interacted with but 
in those he inspired and touched as well.
</p>

<p>
Our thoughts are with his family.
</p>

<h2>About Debian</h2>
<p>The Debian Project is an association of Free Software developers who
volunteer their time and effort in order to produce a completely free
operating system known as Debian.</p>

<h2>Contact Information</h2>
<p>For further information, please visit the Debian web pages at
<a href="$(HOME)/">https://www.debian.org/</a> or send mail to
&lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt;.</p>
