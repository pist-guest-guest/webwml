#use wml::debian::template title="Porte IA-64" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/ia64/menu.inc"
#use wml::debian::translation-check translation="6fa2545407268819444a4d93c89374f5ab40a6a0"

<h1>Debian para IA-64</h1>

<h2>Status</h2>

<p>
IA-64 é uma arquitetura suportada pelo Debian desde o Debian 3.0 (woody) até o
Debian 7 (wheezy)

<p>
Se você gostaria de ajudar, comece assinando a
<a href="#mailinglist">lista de discussão debian-ia64</a>.

<p>
Os canais Debian normais para aquisição de mídia de instalação agora incluem
imagens de CD ia64.

<h2>Versões da Bios</h2>

<p>
Todos os sistemas ia64 modernos devem funcionar bem.
<p>
É possível que você encontre uma máquina ia64 muito antiga que precise de uma
atualização da BIOS para funcionar bem com o Linux.
Uma combinação específica sobre a qual ouvimos falar é tentar executar novos
kernels em sistemas "Lion" com versões realmente antigas da BIOS.
Para tentar facilitar isso,
aqui está o que sabemos sobre as versões da BIOS que funcionam bem para o Debian
em sistemas ia64 mais antigos:

<ul>
<li> Lion, também conhecida como HP rx4610, versão 99b funciona adequadamente.
<li> BigSur, também conhecida como HP i2000, versão 130 funciona adequadamente.
</ul>

<p>
Se alguém tem informações sobre versões adequadas de BIOS para outros sistemas
IA-64 que estão rodando o Debian com sucesso, nos informe na lista debian-ia64!</p>

<h2>Contatos</h2>

<p>
Os instigadores do porte IA-64 foram Bdale Garbee e Randolph Chung.
A melhor maneira de fazer perguntas atualmente é através da lista de discussão.

<h2><a name="mailinglist">Lista de discussão</a></h2>

<p>
Para assinar a lista de discussão para este porte, envie uma mensagem contendo
a palavra "subscribe" como assunto para
<email "debian-ia64-request@lists.debian.org"> ou use a
<a href="https://lists.debian.org/debian-ia64/">página web das listas</a>.
Você também pode navegar e procurar no
<a href="https://lists.debian.org/debian-ia64/">arquivo das listas</a>.
</p>
