#use wml::debian::template title="Traduzindo páginas web do Debian" BARETITLE=true
#use wml::fmt::verbatim
#use wml::debian::translation-check translation="e8ffe40558025c3ab3775f58c330140e50b28277"

<p>Para tornar o trabalho dos(as) tradutores(as) o mais fácil possível, as
páginas são geradas de modo um pouco diferente ao que muitos de vocês estão
acostumados(as). As páginas web são realmente geradas usando fontes que são
nomeadas com
<a href="https://www.shlomifish.org/open-source/projects/website-meta-language/"><tt>wml</tt></a>.
Há diretórios separados para cada idioma.</p>

<p>Se você planeja começar uma tradução inteiramente nova do site web do
Debian, veja a <a href="#completenew">seção sobre começar uma tradução
nova</a>.</p>

<h2><a name="singlepages">Traduzindo páginas individuais</a></h2>

<p>Usamos WML para separar o conteúdo específico de uma página dos
elementos comuns entre múltiplas páginas. Isso significa que o(a) tradutor(a)
precisa editar alguns arquivos fonte WML em vez dos arquivos HTML. Por
favor, <a href="using_git">use o git</a> para obter os fontes atualizados.
Você precisará pegar pelo menos dois diretórios: <tt>webwml/english/</tt>
e <tt>webwml/<var>&lt;idioma&gt;</var>/</tt>.</p>

<p>Para traduzir uma única página do inglês para seu idioma, o arquivo .wml
original deve ser traduzido e colocado dentro do diretório do outro idioma.
O caminho relativo e o nome precisam ser os mesmos do diretório inglês para
que os links continuem funcionando.</p>

<h3>Cabeçalhos de tradução</h3>
<p>É fortemente recomendado que o(a) tradutor(a) inclua uma linha adicional
aos cabeçalhos depois do último <code>#use</code> para registrar o commit
exato do arquivo original que foi traduzido, para que a
<a href="uptodate">atualização seja mais fácil</a>. A linha se parece com isto:
<kbd>#use wml::debian::translation-check translation="<var>&lt;git_commit_hash&gt;</var>"</kbd>
Por favor, observe que se você gerar um arquivo a ser traduzido usando a
ferramenta <tt>copypage.pl</tt> (o que é bastante recomendado), o hash do
commit do git será gerado automaticamente. O uso de <tt>copypage.pl</tt> será
explicado mais para frente.</p>

<p>Algumas equipes de tradução também usam essa linha para marcar o(a)
tradutor(a) oficial de cada página web. Fazendo essa marcação, você receberá
e-mails automáticos quando as páginas que você mantém são atualizadas em inglês,
e que precisam de sua atenção na atualização da tradução. Para isso,
simplesmente adicione o seu nome como(a) mantenedor(a)(a) no fim da linha
<code>#use</code>, deixando-a parecida com esta:
<kbd>#use wml::debian::translation-check translation="<var>git_commit_hash</var>" maintainer="<var>seu nome</var>"</kbd>.
O <tt>copypage.pl</tt> fará isso automaticamente se você configurar a variável
de ambiente <tt>DWWW_MAINT</tt>
ou usar o parâmetro de linha de comando <tt>-m</tt>.
</p>

# (comentário do original em inglês)
# Eu removi as descrições específicas sobre cvs devido à transição do cvs para o
# git. Ajude a atualizar as instruções, se possível.
#<p>Você também precisa explicar para o robô quem é você, com qual
#frequência você deseja receber mensagens automáticas e com qual conteúdo.
#Para fazê-lo, edite (ou deixe que seu coordenador o faça) o arquivo
#webwml/<var>idioma</var>/international/<var>idioma</var>/translator.db.pl
#no CVS. A sintaxe deve ser inteligível e você pode usar o arquivo da
#equipe francesa como modelo se ele não existe para o seu idioma ainda.
#O robô pode enviar vários tipos de informações, e você pode escolher a
#frequência com a qual elas serão enviadas para você. Os tipos diferentes
#de informação são:
#</p>

#<ul>
# <li><b>summary</b>: um sumário de quais documentos estão desatualizados</li>
# <li><b>logs</b>: o "cvs log" entre a versão traduzida e a atual</li>
# <li><b>diff</b>: "cvs diff"</li>
# <li><b>tdiff</b>: o script tentará encontrar as partes do texto traduzido
#     modificadas pelo patch inglês.</li>
# <li><b>file</b>: adiciona a versão atual do arquivo a ser traduzido</li>
#</ul>

#<p>Para cada uma delas, o valor deve estar entre: 0 (nunca), 1 (mensalmente),
#   2 (semanalmente) ou 3 (diariamente).</p>

#<p>Um exemplo poderia ser:
#</p>

#<verbatim>
#                'Martin Quinson' => {
#                        email       => 'martin.quinson@tuxfamily.org',
#                        summary     => 3,
#                        logs        => 3,
#                        diff        => 3,
#                        tdiff       => 0,
#                        file        => 0
#                },
#</verbatim>

<p>O cabeçalho da página web pode ser facilmente produzido usando o
script <tt>copypage.pl</tt> no diretório raiz webwml. O script
copiará a página para o lugar correto, criará diretórios e makefiles
se necessário e adicionará o cabeçalho necessário automaticamente.
Você será avisado(a) se uma página a ser copiada já existe no repositório, seja
porque a página foi removida do repositório (por estar bastante desatualizada),
ou porque alguém já enviou uma tradução e a sua cópia local do repositório não
está atualizada.
</p>

<p>Para começar a usar o <tt>copypage.pl</tt> você deve primeiro configurar
o arquivo <tt>language.conf</tt> no diretório raiz <tt>webwml</tt>, que será
usado para determinar o idioma para o qual você está traduzindo. Esse arquivo
precisa de apenas duas linhas: na primeira consta o nome do idioma (como
<tt>portuguese</tt>) e na segunda linha pode ser opcionalmente colocado o nome do(a)
tradutor(a) que mantém a página. Você também pode definir o idioma através
do uso da variável de ambiente <tt>DWWW_LANG</tt> e usar a variável de
ambiente <tt>DWWW_MAINT</tt> para colocar seu nome que será adicionado ao
cabeçalho dos arquivos wml gerados, como o(a) mantenedor(a) da tradução. Ou uma
terceira possibilidade, você pode passar o idioma e (opcionalmente) o(a)
mantenedor(a) por linha de comando via <tt>-l german -m "Donald Duck"</tt> e
assim não utilizar o arquivo language.conf. Existem outros recursos disponíveis
no script, apenas execute-o sem nenhum argumento para obter a ajuda.
</p>

<p>Após ter executado, por exemplo, <kbd>./copypage.pl <var>file</var>.wml</kbd>,
traduza o texto original dentro do arquivo. Comentários nos arquivos
indicarão se há itens que não devem ser traduzidos; respeite-os.
Não faça alterações desnecessárias na formatação; se alguma
coisa precisa ser corrigida, a correção provavelmente deve ser feita
no arquivo original.</p>

<h3>Construção e publicação das páginas</h3>

<p>Como nós usamos <a href="content_negotiation">negociação de conteúdo</a>,
arquivos HTML não são nomeados <tt><var>arquivo</var>.html</tt>; são nomeados
<tt><var>arquivo</var>.<var>&lt;idioma&gt;</var>.html</tt>, onde
<var>&lt;idioma&gt;</var> é o código de duas letras para o idioma, de acordo
com a
<a href="https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes">ISO 639</a>
(por exemplo, <tt>pt</tt> para português).</p>

<p>Você pode construir o HTML a partir do WML executando
<kbd>make <var>arquivo</var>.<var>&lt;idioma&gt;</var>.html</kbd>.
Se funcionar, verifique se a sintaxe está completamente válida com
<kbd>weblint <var>arquivo</var>.<var>&lt;idioma&gt;</var>.html</kbd>.</p>

<p>NOTA: As páginas web são frequentemente reconstruídas no servidor www-master,
baseando-se na fonte wml no git. Esse processo é em grande parte resistente
a erros. No entanto, se você colocar um arquivo quebrado no primeiro nível de
sua tradução (por exemplo, o arquivo index.wml no nível mais alto da estrutura),
ele quebrará o processo de construção e interromperá todas as outras
atualizações do site web. Por favor, preste atenção especial a esses
arquivos.</p>

<p>Uma vez que a página esteja pronta para ser publicada, você pode enviá-la
ao git. Se você tem as permissões para fazê-lo, por favor, envie os commits para
o <a href="https://salsa.debian.org/webmaster-team/webwml">repositório webwml do git</a>;
se não, envie para <a
href="translation_coordinators"> alguém com acesso de escrita ao
repositório</a>.</p>

<h2><a name="completenew">Começando uma tradução nova</a></h2>

<p>Se você gostaria de começar a tradução das páginas web do Debian
em um novo idioma, envie uma mensagem (em inglês) para
<a href="mailto:webmaster@debian.org">webmaster@debian.org</a>.

<p>Antes de qualquer coisa, clone nossa árvore fonte como descrito <a
href="using_git">na nossa página de introdução ao git</a>.</p>

<p>Após fazer um git checkout, comece criando um novo diretório para sua
tradução no nível mais alto da estrutura, junto com o english/ e outros. O nome
do diretório de tradução deve estar em inglês e completamente em letras
minúsculas (por exemplo, <q>german</q>, não <q>Deutsch</q>).</p>

<p>Copie os arquivos <tt>Make.lang</tt> e <tt>.wmlrc</tt> do diretório
english/ para o diretório da nova tradução. Esses arquivos são essenciais
para a construção de sua tradução a partir dos arquivos WML. Eles foram
projetados de forma que, depois de copiá-los para o diretório do novo idioma,
você só precise alterar estas coisas:</p>

<ol>
  <li>A variável LANGUAGE deve ser alterada no arquivo <tt>Make.lang</tt>.</li>

  <li>As variáveis CUR_LANG, CUR_ISO_LANG e CHARSET devem ser alteradas
      no arquivo <tt>.wmlrc</tt>. Adicione CUR_LOCALE caso precise
      disso para ordenação.</li>

  <li>Alguns idiomas podem precisar de processamento extra para lidar com
      o conjunto de caracteres (<q>charset</q>). Isso pode ser feito usando
      as opções --prolog e --epilog do wml. Use as variáveis WMLPROLOG e
      WMLEPILOG no <tt>Make.lang</tt> para fazê-lo.</li>

  <li>A variável LANGUAGES deve ser alterada no arquivo
      <tt>webwml/Makefile</tt> no nível mais alto da estrutura, para que seu
      idioma seja construído junto com os outros em www.debian.org. Nós
      preferiríamos que você deixasse essa alteração em particular para os
      webmasters, porque a sua tradução pode estar quebrada sem que você esteja
      ciente, o que poderia parar o processo de construção do resto de nosso
      site web.</li>
</ol>

<p>Depois que isso estiver feito, coloque a seguinte linha em um novo arquivo
chamado "Makefile" nesse diretório:

<pre>
<protect>include $(subst webwml/<var>seudiretóriodoidioma</var>,webwml/english,$(CURDIR))/Makefile</protect>
</pre>

<p>(Substitua <var>seudiretóriodoidioma</var> com o nome do diretório de seu
idioma, claro).</p>

<p>Agora crie um subdiretório em seu diretório de idioma chamado "po",
e copie o mesmo Makefile para esse subdiretório (<kbd>cp ../Makefile .</kbd>).
</p>

<p>No diretório po/, execute <kbd>make init-po</kbd> para gerar o conjunto
inicial de arquivos *.po.</p>

<p>Agora que o esqueleto está configurado, você pode começar a adicionar
suas traduções em nossas tags WML compartilhadas usadas nos modelos
(<q>templates</q>). Os primeiros modelos que você deve traduzir são aqueles
que aparecem em todas as páginas web, como as palavras-chave do cabeçalho, as
entradas da barra de navegação e o rodapé.</p>

# A página <a href="using_wml">using WML</a> tem mais informações sobre isto.

<p>Comece traduzindo o arquivo <code>po/templates.<var>xy</var>.po</code>
(onde <var>xy</var> é o código de duas letras de seu idioma). Para cada
<code>msgid "<var>alguma coisa</var>"</code> há inicialmente um
<code>msgstr ""</code>. Você deve colocar a tradução de <var>alguma coisa</var>
entre aspas duplas depois de <code>msgstr</code>.</p>

<p>Você não tem que traduzir todas as strings em todos os arquivos .po,
somente aquelas que suas páginas traduzidas realmente precisam. Para ver
se você precisa traduzir uma string, leia os comentários no arquivo .po
acima de cada <code>msgid</code>. Se o arquivo referenciado estiver em
<tt>english/template/debian</tt>, então você provavelmente deve traduzi-la.
Se não, você pode deixá-la para depois, quando você traduzir a seção
relevante das páginas web que a demandam.</p>

<p>O objetivo dos arquivos po/ é tornar as coisas mais fáceis para os(as)
tradutores(as), de forma que eles(as) (quase) nunca tenham que editar algo no
diretório <tt>english/template/debian</tt>.
Se você encontrar qualquer coisa errada com a forma como algo está
configurado no diretório template, por favor, tenha certeza que o problema
pode ser corrigido de maneira genérica (sinta-se à vontade para pedir para
que outra pessoa faça isso por você), em vez de adicionar traduções nos
modelos, o que (usualmente) é um grande problema.</p>

<p>Se você não tem certeza se o que fez está correto, pergunte
na lista de discussão debian-www antes de fazer o commit.</p>

<p>Nota: se você descobrir que precisa fazer qualquer outra alteração,
envie uma mensagem para debian-www dizendo o que você alterou e porque,
para que o problema seja corrigido.

<p>Depois que o esqueleto dos templates estiver feito, você pode começar
com a tradução da página inicial e outros arquivos *.wml. Para uma lista
de arquivos que devem ser traduzidos primeiro, veja
<a href="translation_hints">a página de dicas</a>.
Traduza páginas *.wml como descrito <a href="#singlepages">no
topo desta página</a>.</p>

<h3>Revivendo traduções desatualizadas</h3>

<p>Como descrito em <a href="uptodate">como manter as traduções atualizadas</a>,
traduções desatualizadas do site web podem ser automaticamente removidas quando
um longo período de tempo se passou sem uma atualização.</p>

<p>Se você descobrir que alguns arquivos foram removidos há algum tempo e você
gostaria de verificá-los para uma futura edição, você pode procurá-los através
do histórico de commits usando os comandos padrões do git.</p>

<p>Por exemplo, se o arquivo apagado é o "deleted.wml", você pode procurar no
histórico rodando:</p>

<verbatim>
   git log --all --full-history -- <caminho/para/arquivo/deleted.wml>
</verbatim>

<p>Você vai encontrar o commit exato que removeu o arquivo em questão junto
à string do hash do commit. Para exibir as informações detalhadas sobre a
alteração feita para o arquivo neste commit, você pode usar o subcomando
<code>git show</code>:</p>

<verbatim>
  git show <COMMIT_HASH_STRING> -- <caminho/para/arquivo/deleted.wml>
</verbatim>

<p>Se o commit é exatamente aquele que apagou o arquivo, pode pode restaurar o
arquivo para o espaço de trabalho usando <code>git checkout</code>:</p>

<verbatim>
  git checkout <COMMIT_HASH_STRING>^ -- <caminho/para/arquivo/deleted.wml>
</verbatim>

<p>Uma vez feito esse procedimento, você terá que atualizar o documento antes de
enviá-lo (check in). Senão ele poderá ser removido.</p>


<h3>O resto da história</h3>

<p>A descrição acima provavelmente será o suficiente para você
começar. Posteriormente, você pode ver os seguintes documentos
que dão explicações mais detalhadas e informações adicionais.</p>

<ul>
<li>Vários <a href="examples">exemplos</a> são fornecidos para dar uma
    ideia clara de como começar.</li>
<li>Várias questões comuns são respondidas e dicas de grande ajuda são
    dadas na página de <a href="translation_hints">dicas de tradução</a>.</li>
<li>Nós temos mecanismos para ajudar a <a href="uptodate">manter as
    traduções atualizadas</a>.</li>
<li>Para ver o estado da sua tradução e como ela está em comparação a
    outras, verifique a página de <a href="stats/">estatísticas</a>.</li>
</ul>

<p>Esperamos que o trabalho que nós realizamos torne a tradução
das páginas o mais fácil possível. Como já mencionado, se você tiver
qualquer questão, pergunte na lista de discussão <a
href="mailto:debian-www@lists.debian.org">debian-www</a>.</p>

<p>
Para dúvidas sobre como traduzir as páginas web para o português, pergunta na
lista de discussão
<a href="mailto:debian-l10n-portuguese@lists.debian.org">debian-l10n-portuguese</a>
</p>
