#use wml::debian::template title="Debian GNU/Linux 5.0 -- Errata" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="256bbbf3eb181693a0ebf3daf93179f79f7e38a0"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>



<toc-add-entry name="known_probs">Problemas conhecidos</toc-add-entry>

<dl>
<dt>Aviso sobre a chave 4D270D06F42584E6 ao atualizar</dt>
<dd>
<p>Depois de adicionar o lenny ao seu sources.list para atualização, você
provavelmente verá um aviso sobre a falta da chave 4D270D06F42584E6. Isso
ocorre porque o arquivo Release para o lenny é assinado com duas chaves, uma
que está disponível no etch e outra que

<ifneq <current_release_etch> 4.0r7 "

não foi enviada inicialmente com ele. O aviso é inofensivo porque ter uma
chave para verificar o arquivo Release é suficiente, mas apenas atualizar seu
sistema etch mais uma vez antes de atualizar para o lenny deve fazer com que o
aviso desapareça (versão 2009.01.31 do pacote <tt>debian-archive-keyring</tt>
deve ser instalado).</p>

" "

não é.</p>

<p>O aviso é inofensivo, porque a chave disponível no etch é suficiente para
verificar o arquivo Release, e o aviso desaparecerá assim que você tiver
atualizado para o lenny. No entanto, se você está preocupado(a) com isso, ou
gostaria de se livrar do aviso porque Lenny permanecerá no seu sources.list por
um tempo, você deve atualizar sua versão do pacote
<tt>debian-archive-keyring</tt> para 2009.01.31 ou posterior. Você pode obtê-lo
nos seguintes locais:</p>

<ul>
<li>diretamente a partir de <a href="https://packages.debian.org/lenny/debian-archive-keyring">packages.debian.org</a></li>
<li>a partir do repositório <em>etch-proposed-updates</em>, adicionando isso no
seu sources.list:
<pre>
  deb http://archive.debian.org/debian etch-proposed-updates main
</pre>
</li>
<li>a partir dos repositórios volatile (você não vai querer fazer isso se você
já tiver o volatile no seu sources.list)</li>
</ul>
<p>Está planejada uma nova versão pontual do etch, incluindo uma atualização
do pacote <tt>debian-archive-keyring</tt>.</p>

" />

</dd>
</dl>

<toc-add-entry name="security">Problemas de segurança</toc-add-entry>

<p>A equipe de segurança Debian emite atualizações para pacotes na versão
estável (stable), nos quais eles(as) identificaram problemas relacionados à
segurança. Por favor consulte as <a href="$(HOME)/security/">páginas de segurança</a>
para obter informações sobre quaisquer problemas de segurança identificados no
<q>lenny</q>.</p>

<p>Se você usa o APT, adicione a seguinte linha ao <tt>/etc/apt/sources.list</tt>
para poder acessar as atualizações de segurança mais recentes:</p>

<pre>
  deb http://security.debian.org/ lenny/updates main contrib non-free
</pre>

<p>Depois disso, execute <kbd>apt-get update</kbd> seguido por
<kbd>apt-get upgrade</kbd>.</p>


<toc-add-entry name="pointrelease">Lançamentos pontuais</toc-add-entry>

<p>Às vezes, no caso de vários problemas críticos ou atualizações de segurança,
a versão já lançada é atualizada. Geralmente, as atualizações são indicadas
como lançamentos pontuais.</p>

<ul>
  <li>A primeira versão pontual, 5.0.1, foi lançada em
      <a href="$(HOME)/News/2009/20090411">11 de abril de 2009</a>.</li>
  <li>A segunda versão pontual, 5.0.2, foi lançada em
      <a href="$(HOME)/News/2009/20090627">27 de junho de 2009</a>.</li>
  <li>A terceira versão pontual, 5.0.3, foi lançada em
      <a href="$(HOME)/News/2009/20090905">5 de setembro de 2009</a>.</li>
  <li>A quarta versão pontual, 5.0.4, foi lançada em
      <a href="$(HOME)/News/2010/20100130">30 de janeiro de 2010</a>.</li>
  <li>A quinta versão pontual, 5.0.5, foi lançada em
      <a href="$(HOME)/News/2010/20100626">26 de junho de 2010</a>.</li>
  <li>A sexta versão pontual, 5.0.6, foi lançada em
      <a href="$(HOME)/News/2010/20100904">4 de setembro de 2010</a>.</li>
  <li>A sétima versão pontual, 5.0.7, foi lançada em
      <a href="$(HOME)/News/2010/20101127">27 de novembro de 2010</a>.</li>
  <li>A oitava versão pontual, 5.0.8, foi lançada em
      <a href="$(HOME)/News/2011/20110122">22 de janeiro de 2011</a>.</li>
  <li>A nona versão pontual, 5.0.9, foi lançada em
      <a href="$(HOME)/News/2011/20111001">1 de outubro de 2011</a>.</li>
  <li>A décima versão pontual, 5.0.10, foi lançada em
      <a href="$(HOME)/News/2012/20120310">10 de março de 2012</a>.</li>
</ul>

<ifeq <current_release_lenny> 5.0.0 "

<p>Ainda não há lançamentos pontuais para o Debian 5.0</p>" "

<p>Consulte o <a
href=http://archive.debian.org/debian/dists/lenny/ChangeLog>\
ChangeLog</a>
para obter detalhes sobre alterações entre o 5.0.0 e o <current_release_lenny/>.</p>"/>

<p>As correções na versão estável (stable) lançada geralmente passam por um
longo período de teste antes de serem aceitas no repositório.
No entanto, essas correções estão disponíveis no repositório
<a href="http://archive.debian.org/debian/dists/lenny-proposed-updates/">\
dists/lenny-proposed-updates</a> de qualquer espelho do repositório Debian.</p>

<p>Se você usa o APT para atualizar seus pacotes, poderá instalar as
atualizações propostas adicionando a seguinte linha ao
<tt>/etc/apt/sources.list</tt>:</p>

<pre>
  \# Atualizações propostas para a versão 5.0
  deb http://archive.debian.org/debian lenny-proposed-updates main contrib non-free
</pre>

<p>Depois disso, execute <kbd>apt-get update</kbd> seguido por
<kbd>apt-get upgrade</kbd>.</p>


<toc-add-entry name="installer">Sistema de instalação</toc-add-entry>

<p>
Para obter informações sobre erratas e atualizações para o sistema de instalação,
consulte a página <a href="debian-installer/">informações de instalação</a> .
</p>
