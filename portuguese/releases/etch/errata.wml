#use wml::debian::template title="Debian GNU/Linux 4.0 -- Errata" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="28d2aaba8d42e9ca9808154a8580598385f19912"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>



<toc-add-entry name="security">Problemas de segurança</toc-add-entry>

<p>A equipe de segurança do Debian emite atualizações para pacotes na versão
estável (stable), nos quais eles(as) identificaram problemas relacionados à
segurança. Por favor consulte as <a href="$(HOME)/security/">páginas de segurança</a>
para obter informações sobre quaisquer problemas de segurança identificados no
<q>etch</q>.</p>

<p>Se você usa o APT, adicione a seguinte linha ao <tt>/etc/apt/sources.list</tt>
para poder acessar as atualizações de segurança mais recentes:</p>

<pre>
  deb http://security.debian.org/ etch/updates main contrib non-free
</pre>

<p>Depois disso, execute <kbd>apt-get update</kbd> seguido por
<kbd>apt-get upgrade</kbd>.</p>


<toc-add-entry name="pointrelease">Lançamentos pontuais</toc-add-entry>

<p>Às vezes, no caso de vários problemas críticos ou atualizações de segurança,
a versão já lançada é atualizada. Geralmente, as atualizações são indicadas
como lançamentos pontuais.</p>

<ul>
  <li>A primeira versão pontual, 4.0r1, foi lançada em
      <a href="$(HOME)/News/2007/20070817">15 de agosto de 2007</a>.</li>
  <li>A segunda versão pontual, 4.0r2, foi lançada em
      <a href="$(HOME)/News/2007/20071227">26 de dezembro de 2007</a>.</li>
  <li>A terceira versão pontual, 4.0r3, foi lançada em
      <a href="$(HOME)/News/2008/20080217">17 de fevereiro de 2008</a>.</li>
  <li>A quarta versão pontual, 4.0r4, foi lançada em
      <a href="$(HOME)/News/2008/20080726">26 de julho de 2008</a>.</li>
  <li>A quinta versão pontual, 4.0r5, foi lançada em
      <a href="$(HOME)/News/2008/20081023">23 de outubro de 2008</a>.</li>
  <li>A sexta versão pontual, 4.0r6, foi lançada em
      <a href="$(HOME)/News/2008/20081218">18 de dezembro de 2008</a>.</li>
  <li>A sétima versão pontual, 4.0r7, foi lançada em
      <a href="$(HOME)/News/2009/20090210">10 de fevereiro de 2009</a>.</li>
  <li>A oitava versão pontual, 4.0r8, foi lançada em
      <a href="$(HOME)/News/2009/20090408">8 de abril de 2009</a>.</li>
</ul>

<ifeq <current_release_etch> 4.0r0 "

<p>Ainda não há lançamentos pontuais para o Debian 4.0.</p>" "

<p>Consulte o <a
href=http://archive.debian.org/debian/dists/etch/ChangeLog>\
ChangeLog</a>
para ver mais detalhes sobre as mudanças entre o 4.0r0 e o <current_release_etch/>.</p>"/>

<p>As correções na versão estável (stable) lançada geralmente passam por
um longo período de testes antes de serem aceitas no repositório.
No entanto, essas correções estão disponíveis no repositório
<a href="http://archive.debian.org/debian/dists/etch-proposed-updates/">\
dists/etch-proposed-updates</a>
de qualquer espelho do repositório Debian.</p>

<p>Se você usa o APT para atualizar seus pacotes, poderá instalar as
atualizações propostas adicionando a seguinte linha ao
<tt>/etc/apt/sources.list</tt>:</p>

<pre>
  \# Atualizações propostas para a versão 4.0
  deb http://archive.debian.org/debian proposed-updates main contrib non-free
</pre>

<p>Depois disso, execute <kbd>apt-get update</kbd> seguido por
<kbd>apt-get upgrade</kbd>.</p>

<toc-add-entry name="installer">Sistema de instalação</toc-add-entry>

<p>
Para obter informações sobre erratas e atualizações do sistema de instalação,
consulte a página de informações de instalação.
</p>
