#use wml::debian::cdimage title="Baixando as imagens do USB/CD/DVD Debian via HTTP/FTP" BARETITLE=true
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="019351f156c8f8ffc0f7922929f7062fb4b732db"

<p>As seguintes imagens do Debian estão disponíveis para serem baixadas:</p>

<ul>

  <li><a href="#stable">Imagens oficiais de USB/CD/DVD da versão
  <q>estável (stable)</q></a></li>
  
  <li><a href="https://cdimage.debian.org/cdimage/weekly-builds/">Imagens
  oficiais de USB/CD/DVD da versão <q>teste (testing)</q> (<em>regeradas
  semanalmente</em>)</a></li>

</ul>

<p>Veja também:</p>
<ul>

  <li>Uma <a href="#mirrors">lista completa de espelhos <tt>debian-cd/</tt></a></li>

  <li>Para imagens de <q>instalação via rede</q>
  veja a página <a href="../netinst/">instalação via rede</a>.</li>

  <li>Para imagens da versão teste (<q>testing</q>), veja na
  <a href="$(DEVEL)/debian-installer/">página do
  instalador do Debian (<q>Debian-Installer</q>)</a>.</li>

</ul>

<hr />

<h2><a name="stable">Imagens oficiais de USB/CD/DVD da distribuição estável (<q>stable</q>)</a></h2>

<p>Para instalar o Debian em uma máquina sem conexão com a Internet,
é possível usar as imagens de CD/USB (700&nbsp;MB cada) ou as imagens de DVD/USB
(4,4&nbsp;GB cada). Baixe o primeiro arquivo de imagem de CD/USB ou DVD/USB, grave-o
usando um gravador de USB/CD/DVD, e então reinicialize a partir dessa mídia.</p>

<p>O <strong>primeiro</strong> disco de USB/CD/DVD contém todos os arquivos
necessários para instalar um sistema Debian padrão.<br />
</p>

<div class="line">
<div class="item col50">
<p><strong>CD/USB</strong></p>

<p>Os seguintes links apontam para arquivos de imagem que têm até 650&nbsp;MB
de tamanho, tornando-os adequados para gravação em mídia CD-R(W) normal:</p>

<stable-full-cd-images />
</div>
<div class="item col50 lastcol">
<p><strong>DVD/USB</strong></p>

<p>Os seguintes links apontam para arquivos de imagem que têm até 4,4&nbsp;GB
de tamanho, tornando-os adequados para gravação em mídias DVD-R/DVD+R normais
e similares:</p>

<stable-full-dvd-images />
</div><div class="clear"></div>
</div>

<p>Tenha certeza de ter olhado a documentação antes de instalar.
<strong>Se você quer ler somente um único documento</strong> antes da
instalação, leia nosso <a href="$(HOME)/releases/stable/amd64/apa">tutorial de instalação</a>,
um rápido passo a passo do processo de instalação. Outras documentações úteis
incluem:
</p>
<ul>
<li><a href="$(HOME)/releases/stable/installmanual">Guia de Instalação</a>,
    as instruções de instalação detalhadas</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Documentação do
    instalador do Debian (<q>Debian-Installer</q>)</a>, incluindo o FAQ com questões e respostas comuns</li>
<li><a href="$(HOME)/releases/stable/debian-installer/#errata">Errata do
    instalador do Debian (<q>Debian-Installer</q>)</a>, a lista de problemas conhecidos no instalador</li>
</ul>

<hr />

<h2><a name="mirrors">Espelhos registrados do repositório <q>debian-cd</q></a></h2>

<p>Note que <strong>alguns espelhos podem não estar atualizados</strong> &mdash;
a versão atual das imagens de USB/CD/DVD "estáveis" (stable) é <strong><current-cd-release></strong>.

<p><strong>Em caso de dúvida, use o <a
href="https://cdimage.debian.org/debian-cd/">servidor de imagens
de CD</a> na Suécia</strong>.</p>

<p>Você está interessado(a) em oferecer imagens do CD Debian no seu servidor
de arquivos (<q>mirror</q>)? Se sua resposta for sim, veja as
<a href="../mirroring/">instruções sobre como montar um espelho
de imagens de CD (<q>mirror</q>)</a>.</p>

#use wml::debian::countries
#include "$(ENGLISHDIR)/CD/http-ftp/cdimage_mirrors.list"
