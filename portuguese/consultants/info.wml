#use wml::debian::template title="Informações para consultores(as) Debian"
#use wml::debian::translation-check translation="5f70081cb38037d61262d8cb159fb510c9315981"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#policy">Política para a página de consultores(as) Debian</a></li>
  <li><a href="#change">Adições, modificações e remoções de entradas</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Os(As) consultores(as) podem
se comunicar (em inglês) com outros(as) consultores(as) Debian por meio da
<a href="https://lists.debian.org/debian-consultants /">lista de discussão debian-consultants</a>.
A lista não é moderada; a postagem é permitida por qualquer pessoa. A lista de
discussão também fornece um arquivo público.</p>
</aside>

<h2><a id="policy">Política para a página de consultores(as) Debian</a></h2>

<p>
Se você deseja ser listado(a) como um(a) consultor(a) no site web do Debian,
por favor observe as seguintes regras:
</p>

<ul>
  <li><strong>Informações de contato obrigatórias:</strong><br />
    Você deve fornecer um endereço de e-mail ativo e responder a e-mails
    enviados a você dentro de pelo menos quatro semanas. Para evitar abusos,
    quaisquer solicitações (adições, remoções ou alterações) devem ser enviadas
    desse mesmo endereço. Para evitar spam, você pode solicitar que seu
    endereço de e-mail seja codificado (por exemplo, <em>debian -dot- consulting
    -at- example -dot- com</em>). Por favor, escreva isso explicitamente na sua
    inscrição. Como alternativa, você pode solicitar que seu endereço de e-mail
    não apareça no site web (embora precisemos de um endereço de e-mail
    funcional para manutenção da lista).
    Se deseja que seu endereço fique oculto na lista, você pode fornecer a URL
    de um formulário de contato do seu site web para que possamos linkar.
    <em>Contact</em> deve ser usado para isso.
  </li>
  <li><strong>Seu site:</strong><br />
    Se você fornecer um link para um site web, esse site web deve ter uma menção
    aos seus serviços de consultoria Debian. Fornecer o link direto em vez de
    apenas a página inicial não é obrigatório se a informação for razoavelmente
    acessível, além de muito apreciada.
  </li>
  <li><strong>Várias cidades/regiões/países:</strong><br />
    Você deve escolher o país (apenas um) em que deseja ser listado(a). Cidades,
    regiões ou países adicionais devem ser listados como parte das informações
    adicionais no seu próprio site.
  </li>
  <li><strong>Regras para desenvolvedores(as) Debian:</strong><br />
    Você não tem permissão para usar seu endereço de e-mail oficial
    <em>@debian.org</em> para a lista de consultores(as). O mesmo vale para o
    seu site web: você não pode usar um domínio oficial <em>*.debian.org</em>
    conforme descrito no <a href="$(HOME)/devel/dmup">DMUP</a> (políticas de uso
    de máquinas do Debian).
  </li>
</ul>

<p>
Se os critérios acima não forem mais atendidos em algum momento futuro, o(a)
consultor(a) deverá receber uma mensagem de aviso de que ele(a) está prestes a
ser removido(a) da lista, a menos que cumpra todos os critérios novamente.
Deve haver um período de carência de quatro semanas.
</p>

<p>
Algumas partes (ou todas) das informações do(a) consultor(a) podem ser
removidas se ele(a) não estiver mais em conformidade com esta política, ou a
critério dos(as) mantenedores(as) da lista.
</p>

<h2><a id="change">Adições, modificações e remoções de entradas</a></h2>

<p>
Se você deseja ser adicionado(a) à lista de consultores(as), por favor envie um
e-mail em inglês para
<a href="mailto:consultants@debian.org"> consultants@debian.org </a>,
fornecendo qualquer uma das seguintes informações que você gostaria que fosse
listada (o endereço de e-mail é obrigatório, todo o resto é opcional a seu
critério):
</p>

<ul>
  <li>País que você quer que seja listado(a)</li>
  <li>Nome</li>
  <li>Empresa</li>
  <li>Endereço</li>
  <li>Telefone</li>
  <li>Fax</li>
  <li>Contato</li>
  <li>E-mail</li>
  <li>URL</li>
  <li>Valores</li>
  <li>Informações adicionais</li>
</ul>

<p>
Um pedido de atualização das informações do(a) consultor(a) deve ser enviado
por e-mail em inglês para
<a href="mailto:consultants@debian.org">consultants@debian.org</a>.
Preferencialmente do endereço de e-mail mencionado na
<a href="https://www.debian.org/consultants/">página dos(as) consultores(as)</a>.
</p>

# tradutores podem, mas não precisam, traduzir o resto - ainda
# BEGIN future version
# fill out the following submission form:</p>
#
# <form method=post action="https://cgi.debian.org/cgi-bin/submit_consultant.pl">
#
# <p>
# <input type="radio" name="submissiontype" value="new" checked>
# New consultant listing submission
# <br />
# <input type="radio" name="submissiontype" value="update">
# Update of an existing consultant listing
# <br />
# <input type="radio" name="submissiontype" value="remove">
# Removal of an existing consultant listing
# </p>
#
# <p>Name:<br />
# <input type="text" name="name" size="50"></p>
#
# <p>Company:<br />
# <input type="text" name="company" size="50"></p>
#
# <p>Address:<br />
# <textarea name="comment" cols=35 rows=4></textarea></p>
#
# <p>Phone:<br />
# <input type="text" name="phone" size="50"></p>
#
# <p>Fax:<br />
# <input type="text" name="fax" size="50"></p>
#
# <p>E-mail:<br />
# <input type="text" name="email" size="50"></p>
#
# <p>URL:<br />
# <input type="text" name="url" size="50"></p>
#
# <p>Rates:<br />
# <textarea name="comment" cols=35 rows=4></textarea></p>
#
# <p>Additional information, if any (<em>in English</em>):<br />
# <textarea name="comment" cols=40 rows=7></textarea></p>
#
# </form>
#
# <p>If you are unable to submit the above for any reason whatsoever,
# please send it via e-mail <em>in English</em> to
# END future version
