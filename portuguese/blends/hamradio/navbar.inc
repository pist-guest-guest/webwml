#use wml::debian::blends::hamradio

{#alternate_navbar#:
   <div id="second-nav">
      <p><a href="$(HOME)/blends/hamradio/">Debian&nbsp;Hamradio&nbsp;Blend</a></p>
    <ul>
      <li><a href="$(HOME)/blends/hamradio/about">Sobre</a></li>
      <li><a href="$(HOME)/blends/hamradio/News/">Arquivo&nbsp;de&nbsp;notícias</a></li>
      <li><a href="$(HOME)/blends/hamradio/contact">Contato</a></li>
      <li><a href="$(HOME)/blends/hamradio/get/">Obtendo&nbsp;o&nbsp;blend</a>
         <ul>
         <li><a href="$(HOME)/blends/hamradio/get/metapackages">Usndo&nbsp;os&nbsp;metapacotes</a></li>
	 </ul>
      </li>
      <li><a href="$(HOME)/blends/hamradio/docs/">Documentação</a>
        <ul>
        <li><a href="<hamradio-handbook-html/>">Debian&nbsp;Hamradio&nbsp;Handbook</a></li>
        </ul>
      </li>
      <li><a href="$(HOME)/blends/hamradio/support">Suporte</a></li>
      <li><a href="$(HOME)/blends/hamradio/dev">Desenvolvimento</a>
        <ul>
        <li><a href="https://wiki.debian.org/DebianHams">Equipe de mantenedores(as) do Hamradio</a></li>
        <li><a href="<hamradio-maintguide-html/>">Guia dos(as) mantenedores(as) do Hamradio</a></li>
        </ul>
      </li>
      <li><a href="https://twitter.com/DebianHamradio"><img src="$(HOME)/blends/hamradio/Pics/twitter.gif" alt="Twitter" width="80" height="15"></a></li>
    </ul>
   </div>
:#alternate_navbar#}
