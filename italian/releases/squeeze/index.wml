#use wml::debian::template title="Informazioni sul rilascio Debian &ldquo;squeeze&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="476ef1924df8fdb75ad78952645435d512788254" maintainer="Luca Monducci"

<p>Debian <current_release_squeeze> è stata rilasciata <a
href="$(HOME)/News/<current_release_newsurl_squeeze/>"><current_release_date_squeeze></a>.
<ifneq "6.0.0" "<current_release>"
  "Debian 6.0.0 è stata inizialmente rilasciata il <:=spokendate('2011-02-06'):>."
/>
Il rilascio includeva molte modifiche significative, descritte nel nostro
<a href="$(HOME)/News/2011/20110205a">comunicato stampa</a> e nelle <a
href="releasenotes">Note di Rilascio</a>.</p>

<p><strong>Debian 6 è stata sostituita da
<a href="../wheezy/">Debian 7 (<q>wheezy</q>)</a>.
Gli aggiornamenti di sicurezza sono stati interrotti dal <:=spokendate('2014-05-31'):>.
</strong></p>

<p><strong>Squeeze ha anche beneficiato del Supporto a Lungo Termine (LTS)
fino alla fine di febbraio 2016. Il LTS era limitato a i386 e amd64. Per
ulteriori informazioni, consultare la <a
href="https://wiki.debian.org/LTS">sezione LTS del Debian Wiki.</a>.
</strong></p>

<p>Per ottenere e installare Debian, consulta la pagina delle informazioni
sull'installazione e la Guida all'installazione. Per effettuare 
l'aggiornamento da una versione precedente di Debian, segui le istruzioni
nelle <a href="releasenotes">Note di Rilascio</a>.</p>

<p>Architetture supportate al rilascio iniziale di stretch:</p>
<ul>
<li><a href="../../ports/i386/">32-bit PC (i386)</a>
<li><a href="../../ports/amd64/">64-bit PC (amd64)</a>
<li><a href="../../ports/armel/">EABI ARM (armel)</a>
<li><a href="../../ports/powerpc/">PowerPC</a>
<li><a href="../../ports/sparc/">SPARC</a>
<li><a href="../../ports/ia64/">Intel Itanium IA-64</a>
<li><a href="../../ports/mipsel/">MIPS (little endian)</a>
<li><a href="../../ports/kfreebsd-amd64/">kFreeBSD 64-bit PC (amd64)</a>
<li><a href="../../ports/kfreebsd-i386/">kFreeBSD 32-bit PC (i386)</a>
<li><a href="../../ports/s390/">IBM S/390</a>
<li><a href="../../ports/mips/">MIPS (big endian)</a>
</ul>

<p>Contrariamente a quanto ci auguriamo, è possibile che esistano alcuni
problemi, nonostante sia dichiarata <em>stabile</em>. Abbiamo compilato <a
href="errata">una lista dei principali problemi noti</a>, e potete sempre
segnalarci altri problemi.</p>
