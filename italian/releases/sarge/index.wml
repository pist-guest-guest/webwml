#use wml::debian::template title="Informazioni sul rilascio Debian &ldquo;sarge&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="c828123435e049f9c2b89a8e80a66d0817b40439" maintainer="Luca Monducci"

<p>Debian GNU/Linux 3.1 (nota anche come <em>sarge</em>) è stata rilasciata
il <:=spokendate('2005-06-06'):>.
Il rilascio includeva molte modifiche significative, descritte nel nostro
<a href="$(HOME)/News/2005/20050606">comunicato stampa</a> e nelle <a
href="releasenotes">Note di Rilascio</a>.</p>

<p><strong>Debian 3.1 è stata sostituita da
<a href="../buster/">Debian GNU/Linux 4.0 (<q>etch</q>)</a>.
Gli aggiornamenti di sicurezza sono stati interrotti dal
<:=spokendate('2008-03-31'):>.</strong></p>

<p>Per ottenere e installare Debian, consulta la pagina delle informazioni
sull'installazione e la Guida all'installazione. Per effettuare 
l'aggiornamento da una versione precedente di Debian, segui le istruzioni
nelle <a href="releasenotes">Note di Rilascio</a>.</p>

<p>Architetture supportate al rilascio iniziale di sarge:</p>
<ul>
<li><a href="../../ports/alpha/">Alpha</a>
<li><a href="../../ports/arm/">ARM</a>
<li><a href="../../ports/hppa/">HP PA-RISC</a>
<li><a href="../../ports/i386/">32-bit PC (i386)</a>
<li><a href="../../ports/ia64/">Intel Itanium IA-64</a>
<li><a href="../../ports/m68k/">Motorola 680x0</a>
<li><a href="../../ports/mips/">MIPS (big endian)</a>
<li><a href="../../ports/mipsel/">MIPS (little endian)</a>
<li><a href="../../ports/powerpc/">PowerPC</a>
<li><a href="../../ports/s390/">IBM S/390</a>
<li><a href="../../ports/sparc/">SPARC</a>
</ul>

<p>Contrariamente a quanto ci auguriamo, è possibile che esistano alcuni
problemi, nonostante sia dichiarata <em>stabile</em>. Abbiamo compilato <a
href="errata">una lista dei principali problemi noti</a>, e potete sempre
segnalarci altri problemi.</p>
