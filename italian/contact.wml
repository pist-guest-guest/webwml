#use wml::debian::template title="Mettersi in contatto con noi" NOCOMMENTS="yes" MAINPAGE="true"
#use wml::debian::translation-check translation="fb050641b19d0940223934350c51061fcb2439a9" maintainer="Ceppo"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#generalinfo">Informazioni generali</a></li>
  <li><a href="#installuse">Installare e usare Debian</a></li>
  <li><a href="#press">Pubblicit&agrave; / Stampa</a></li>
  <li><a href="#events">Eventi / Conferenze</a></li>
  <li><a href="#helping">Aiutare Debian</a></li>
  <li><a href="#packageproblems">Segnalare problemi nei pacchetti Debian</a></li>
  <li><a href="#development">Lo sviluppo di Debian</a></li>
  <li><a href="#infrastructure">Problemi con l'infrastruttura Debian</a></li>
  <li><a href="#harassment">Problemi di molestie</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span>I messaggi iniziali dovrebbero essere inviati in <strong>inglese</strong>, perch&eacute; questa &egrave; la lingua che la maggior parte di noi conosce. Se non fosse possibile, rivolgersi a una delle nostre <a href="https://lists.debian.org/users.html#debian-user">liste di messaggi per gli utenti</a>, che sono disponibili in molte lingue.</p>
</aside>

<p>
Debian &egrave; una grande organizzazione e ci sono molti modi per contattare i membri del progetto e altri utenti. Questa pagina riassume le vie di comunicazione pi&ugrave; utilizzate; non &egrave; in alcun modo esaustiva. Leggere le altre pagine del sito per trovare altri metodi di contatto.
</p>

<p>
Si tenga presente che la maggior parte degli indirizzi email elencati di seguito sono liste di messaggi aperte con archivi pubblici. Leggere l'<a href="$(HOME)/MailingLists/disclaimer">avvertenza</a> prima di inviare qualsiasi messaggio.
</p>

<h2 id="generalinfo">Informazioni generali</h2>

<p>
La maggior parte delle informazioni su Debian &egrave; raccolta nel nostro sito web, <a href="$(HOME)">https://www.debian.org/</a>, quindi si prega di visitarlo e <a href="$(SEARCH)">effettuarvi una ricerca</a> prima di contattarci.
</p>

<p>
Si possono inviare domande sul progetto Debian alla lista di messaggi <email debian-project@lists.debian.org>. Non inviare domande generali su Linux a questa lista; leggere oltre per pi&ugrave; informazioni su altre liste di messaggi.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span><a href="doc/user-manuals#faq">Leggi le nostre domande frequenti</a></button></p>

<aside class="light">
  <span class="fa fa-envelope fa-5x"></span>
</aside>
<h2 id="installuse">Installare e usare Debian</h2>

<p>
Se si &egrave; certi che la documentazione distribuita con il supporto di installazione e sul nostro sito non offrano la soluzione al proprio problema, c'&egrave; una lista di messaggi molto attiva dove gli utenti di Debian e gli sviluppatori possono rispondere alle domande. Qui si possono fare domande di qualsiasi tipo su questi argomenti:
</p>

<ul>
  <li>installazione</li>
  <li>configurazione</li>
  <li>hardware supportato</li>
  <li>amministrazione della macchina</li>
  <li>uso di Debian</li>
</ul>

<p>
<a href="https://lists.debian.org/debian-user/">Iscriversi</a> alla lista e inviare le domande a <email debian-user@lists.debian.org>.
</p>

<p>
Ci sono anche liste di messaggi per gli utenti in lingue diverse dall'inglese. Si vedano le informazioni sulle <a href="https://lists.debian.org/users.html#debian-user">liste di messaggi internazionali</a>.
</p>

<p>
Si possono inoltre consultare gli <a href="https://lists.debian.org/">archivi delle nostre liste di messaggi</a> o <a href="https://lists.debian.org/search.html">cercare</a> all'interno di essi senza iscriversi.
</p>

<p>
Ancora, si possono consultare le nostre liste di messaggi come gruppi di discussione.
</p>

<p>
Se si ritiene di aver trovato un bug nel nostro sistema di installazione, scrivere alla lista di messaggi <email debian-boot@lists.debian.org>. Si pu&ograve; anche inviare una <a href="$(HOME)/releases/stable/i386/ch05s04#submit-bug">segnalazione</a> relativa allo pseudo-pacchetto <a href="https://bugs.debian.org/debian-installer">debian-installer</a>.
</p>

<aside class="light">
  <span class="far fa-newspaper fa-5x"></span>
</aside>
<h2 id="press">Pubblicit&agrave; / Stampa</h2>

<p>
Il <a href="https://wiki.debian.org/Teams/Publicity">Debian publicity team</a> modera notizie e annunci su tutte le risorse ufficiali di Debian, tra cui alcune liste di messaggi, il blog, il sito di microinformazione e i canali ufficiali sui social media.
</p>

<p>
Chi sta scrivendo su Debian e ha bisogno di aiuto o informazioni pu&ograve; contattare il <a href="mailto:press@debian.org">publicity team</a>. Questo &egrave; anche l'indirizzo a cui segnalare notizie per le nostre pagine.
</p>

<aside class="light">
  <span class="fas fa-handshake fa-5x"></span>
</aside>
<h2 id="events">Eventi / Conferenze</h2>

<p>
Gli inviti a <a href="$(HOME)/events/">conferenze</a>, mostre o altri eventi vanno inviati a <email events@debian.org>.
</p>

<p>
Richieste per volantini, poster e partecipazioni in Europa vanno inviate alla lista di messaggi <email debian-events-eu@lists.debian.org>.

<aside class="light">
  <span class="fas fa-hands-helping fa-5x"></span>
</aside>
<h2 id="helping">Aiutare Debian</h2>

<p>
Ci sono molte possibilit&agrave; di aiutare il progetto Debian e contribuire alla distribuzione. Per aiutare, consultare prima di tutto la pagina <a href="devel/join/">Come unirsi a Debian</a>
</p>

<p>
Per gestire un mirror di Debian, visitare la pagina su come  <a href="mirror/">gestire un mirror di Debian</a>. I nuovi mirror possono essere segnalati con <a href="mirror/submit">questo modulo</a>. I problemi con i mirror esistenti vanno segnalati a <email mirrors@debian.org>.
</p>

<p>
Per vendere CD o DVD di Debian, vedere le <a href="CD/vendors/info">informazioni per i distributori di CD</a>. Per essere inseriti nell'elenco dei distributori, usare <a href="CD/vendors/adding-form">questo modulo</a>.

<aside class="light">
  <span class="fas fa-bug fa-5x"></span>
</aside>
<h2 id="packageproblems">Segnalare problemi nei pacchetti Debian</h2>

<p>
Per inviare una segnalazione di bug in un pacchetto Debian, abbiamo un sistema di tracciamento al quale segnalare facilmente il problema. Leggere le <a href="Bugs/Reporting">istruzioni per segnalare bug</a>.
</p>

<p>
Per comunicare con il manutentore di un pacchetto Debian &egrave; possibile usare un alias disponibile per ogni pacchetto. Ogni messaggio inviato a &lt;<var>nome pacchetto</var>&gt;@packages.debian.org sar&agrave; inviato al responsabile di quel pacchetto.
</p>

<p>
Per informare gli sviluppatori di un problema di sicurezza di Debian in modo discreto, inviare un'email a <email security@debian.org>.
</p>

<aside class="light">
  <span class="fas fa-code-branch fa-5x"></span>
</aside>
<h2 id="development">Sviluppo</h2>

<p>
Per le domande sullo sviluppo di Debian abbiamo varie <a href="https://lists.debian.org/devel.html">liste di messaggi</a> dove puoi entrare in contatto con i nostri sviluppatori.
</p>

<p>
C'&egrave; anche una lista di messaggi generale per gli sviluppatori: <email debian-devel@lists.debian.org>. Per iscriversi, visitare  <a href="https://lists.debian.org/debian-devel/">questa pagina</a>.
</p>

<aside class="light">
  <span class="fas fa-network-wired fa-5x"></span>
</aside>
<h2 id="infrastructure">Problemi con le infrastrutture Debian</h2>

<p>Per segnalare un problema legato ad un servizio Debian, di solito si pu&ograve; <a href="Bugs/Reporting">segnalare un bug</a> per lo <a href="Bugs/pseudo-packages">pseudo-pacchetto</a> appropriato.</p>

<p>
In alternativa, si pu&ograve; inviare un'email a uno di questi indirizzi:

<define-tag btsurl>package: <a href="https://bugs.debian.org/%0">%0</a></define-tag>

<dl>
<dt>Autori delle pagine web</dt>
  <dd><btsurl www.debian.org><br />
      <email debian-www@lists.debian.org></dd>
#include "$(ENGLISHDIR)/devel/website/tc.data"
<ifneq "$(CUR_LANG)" "English" "
<dt>Traduttori delle pagine web</dt>
  <dd><: &list_translators($CUR_LANG); :></dd>
">
<dt>Amministratori delle liste di messaggi e dei relativi archivi</dt>
  <dd><btsurl bugs.debian.org><br />
      <email listmaster@lists.debian.org></dd>
<dt>Amministratori del sistema di tracciamento dei bug</dt>
  <dd><btsurl bugs.debian.org><br />
      <email owner@bugs.debian.org></dd>
</dl>

<aside class="light">
  <span class="fas fa-umbrella fa-5x"></span>
</aside>
<h2 id="harassment">Problemi di molestie</h2>

<p>
Debian &egrave; una comunit&agrave; di persone che tengono al rispetto. Le vittime di comportamenti inappropriati, chi &egrave; stato danneggiato da qualcuno all'interno del progetto e coloro che sentono di essere stati molestati dovrebbero contattare il community team: <email community@debian.org>.
</p>
