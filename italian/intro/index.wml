#use wml::debian::template title="Introduzione a Debian" MAINPAGE="true" FOOTERMAP="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="1cf898ff877f907fd4f3471e33d7d506701907f3"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div class="row">

  <!-- left column -->
  <div class="column column-left" id="community">
    <div style="text-align: center">
      <span class="fa fa-users fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <a id="community"></a>
      <h2>Debian è un comunità</h2>
      <p>
        Migliaia di volontari di ogni parte del mondo lavorano assieme sul
        sistema operativo Debian, dando priorità al Software Libero e Open
        Source. Incontra il progetto Debian.
      </p>
    </div>

    <div style="text-align: left">
      <ul>
  <li>
    <a href="people">Le persone:</a>
    Chi siamo e cosa facciamo
  </li>
  <li>
    <a href="philosophy">La nostra filosofia:</a>
    Perché e come lo facciamo
  </li>
  <li>
    <a href="../devel/join/">Partecipa:</a>
    Come diventare un collaboratore
  </li>
  <li>
    <a href="help">Contribuisci:</a>
	Come puoi aiutare Debian
  </li>
  <li>
    <a href="../social_contract">Contratto sociale Debian:</a>
    I nostri principi
  </li>
  <li>
    <a href="diversity">Tutti sono i benvenuti:</a>
	Dichiarazione Debian sulla diversità
  </li>
  <li>
    <a href="../code_of_conduct">Per i partecipanti:</a>
	Codice di condotta Debian
  </li>
  <li>
    <a href="../partners/">Partner:</a>
    Aziende e organizzazioni che sostengono il progetto Debian
  </li>
  <li>
    <a href="../donations">Donazioni</a>
	Come diventare uno sponsor del progetto Debian
  </li>
  <li>
    <a href="../legal/">Questioni legali:</a>
	Licenze, marchi, policy sulla privacy, policy sui brevetti, ecc.
  </li>
  <li>
    <a href="../contact">Contatti:</a>
	Come mettersi in contatti con noi
  </li>
      </ul>
    </div>
  </div>

  <!-- right column -->
  <div class="column column-right" id="software">
    <div style="text-align: center">
      <span class="fa fa-desktop fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <a id="software"></a>
      <h2>Debian è un Sistema Operativo</h2>
      <p>
        Debian è un sistema operativo libero, sviluppato e gestito dal progetto
        Debian. Una distribuzione Linux libera con migliaia di applicazioni per
        le necessità degli utenti.
      </p>
    </div>

    <div style="text-align: left">
      <ul>
  <li>
    <a href="../distrib">Scarica:</a>
    Dove ottenere Debian
  </li>
  <li>
    <a href="why_debian">Perché Debian:</a>
    Motivi per scegliere Debian
  </li>
  <li>
    <a href="../support">Assistenza:</a>
    Dove trovare aiuto
  </li>
  <li>
    <a href="../security">Sicurezza:</a>
    Avvertenze recenti e fonti di informazioni sulla sicurezza
  </li>
  <li>
    <a href="../distrib/packages">Software:</a>
    Fai ricerche e naviga nel lungo elenco dei pacchetti Debian
  </li>
  <li>
    <a href="../doc">Documentazione:</a>
	Guida all'installazione, FAQ, HOWTO, Wiki e altro ancora
  </li>
  <li>
    <a href="../Bugs">Sistema di Tracciamento dei Bug (BTS):</a>
	Come segnalare un bug, documentazione del BTS
  </li>
  <li>
    <a href="https://lists.debian.org/">Liste di messaggi:</a>
	Raccolta delle liste Debian per utenti, sviluppatori, ecc.
  </li>
  <li>
    <a href="../blends">Pure Blends:</a>
    Metapacchetti per usi specifici
  </li>
  <li>
    <a href="../devel">Angolo degli sviluppatori:</a>
    Informazioni rivolte principalmente agli sviluppatori Debian
  </li>
  <li>
    <a href="../ports">Port/Architetture:</a>
    Supporto per Debian su varie architetture di CPU
  </li>
  <li>
    <a href="search">Cerca:</a>
	Informazioni su come usare il motore di ricerca Debian
  </li>
  <li>
    <a href="cn">Lingue:</a>
	Impostazioni della lingua per il sito di Debian
  </li>
      </ul>
    </div>
  </div>

</div>
