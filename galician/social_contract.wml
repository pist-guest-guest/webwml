#use wml::debian::template title="Contrato social de Debian" BARETITLE=true
#use wml::debian::translation-check translation="79d1ad80a8ac84afa8c8a224b81fb50c327e6b4f" maintainer="Jorge Barreiro González"

{#meta#:
<meta name="keywords" content="social contract, dfsg, social contract,
dfsg, dfsg, debian social contract, dfsg">
:#meta#}

#  Original document: contract.html
#  Author           : Manoj Srivastava ( srivasta@tiamat.datasync.com )
#  Created On       : Wed Jul  2 12:47:56 1997

<p>Version 1.2 ratificada no 1 de outubro de 2022.</p>

<p>Substitúe a
<a href="social_contract.1.1">versión 1.1</a>, ratificada o 26 de abril do 2004,
e a <a href="social_contract.1.0">versión 1.0</a>, ratificada o 5 de xullo de 1997.
</p>

<p>O proxecto Debian, creador do sistema Debian, creou o <strong>Contrato Social
de Debian</strong>. As <a href="#guidelines">directrices de software libre
de Debian (DFSG)</a>, que son parte do contrato e foron inicialmente
deseñadas como un conxunto de obrigacións que aceptamos acatar, foron adoptadas
 pola comunidade de software libre como a base da 
<a href="https://opensource.org/docs/osd">definición de código aberto</a>.</p>

<hr />
    <h2><q>Contrato social</q> coa comunidade de software libre</h2>

    <ol>
      <li>
	<strong>Debian permanecerá 100% libre</strong>
	<p>
	  Proporcionamos as directrices que usamos para determinar se un traballo
	  é <q><em>libre</em></q> no documento titulado <q><cite>The Debian Free Software
Guidelines (As directrices de software libre de Debian)</cite></q>. Prometemos que o sistema Debian e
	  todos os seus compoñentes serán libres de acordo con esas
	  directrices. Apoiaremos a xente que cree ou use
	  tanto traballos libres como non libres en Debian. Nunca faremos que o
	  sistema requira un compoñente non libre.
	</p>
      </li>
      <li><strong>Contribuiremos á comunidade de software libre</strong>
	<p>
	  Cando escribamos novos compoñentes do sistema Debian,
	  licenciarémolos de forma consistente coas Directrices de Software Libre de Debian.
	  Faremos o mellor sistema que poidamos, de forma que os traballos libres sexan
	  amplamente distribuídos e usados. Comunicaremos os arranxos de fallos, melloras e
	  peticións dos usuarios aos <q><em>autores orixinais</em></q> dos traballos
	  incluídos no noso sistema.
	</p>
      </li>
      <li><strong>Non agocharemos os problemas</strong>
	<p>
	  Manteremos a base de datos de informes de fallos completa aberta ao público
	  para a súa consulta en todo momento. Os informes que a xente cubra en liña serán
	  visíbeis aos demais sen demora.
	</p>
      </li>
      <li><strong>As nosas prioridades son os usuarios e o software libre</strong>
	<p>
	  Guiarémonos polas necesidades dos nosos usuarios e da comunidade de
	  software libre. Poremos o seu interese como a primeira
	  das nosas prioridades. Apoiaremos as necesidades dos nosos usuarios para
	  operar en moitos tipos diferentes de ambientes de
	  computación. Non poremos obxección a traballos non libres
	  pensados para ser usados en sistemas Debian, nin trataremos de
	  cobrarlle á xente que cree ou use eses traballos. Permitiremos
	  a outros crear distribucións contendo o sistema
	  Debian e outros traballos, sen taxas pola nosa parte. En
	  persecución destes obxectivos proporcionaremos un sistema integrado
	  integrado de materiais de alta calidade sen restricións legais
	  que preveñan estes usos do sistema.
	</p>
      </li>
      <li><strong>Traballos que non cumpren os nosos estándares de software libre</strong>
	<p>
	  Sabemos que algúns dos nosos usuarios precisan do uso de
	  traballos que non seguen as Directrices de Software Libre
	  de Debian. Para estes traballos creamos as áreas
	  <q><code>contrib</code></q> e <q><code>non-free</code></q> no noso
	   arquivo. Os paquetes nestas áreas non son parte do
	  sistema Debian, inda que estean configuradas para ser usadas
	  con Debian. Pedimos aos creadores de CDs que lean
	  as licenzas dos paquetes nestas áreas e determinen se
	  poden distribuír os paquetes nos seus CDs. Desta maneira,
	  inda que os traballos non libres non son parte de Debian, apoiaremos
	  o seu uso e proporcionamos unha infraestrutura para os paquetes non libres
	  (como o sistema de seguimento de fallos ou as listas de correo). Os discos
          oficiais de Debian, de forma excepcional, poderán incluír
          «firmware» de fóra do sistema Debian para permitir o uso de Debian cos dispositivos
          que así o precisen.
	</p>
      </li>
    </ol>
<hr />
<h2 id="guidelines">As Directrices de Software Libre de Debian (DFSG)</h2>
<ol>
   <li><p><strong>Libre Redistribución</strong></p>
     <p>A licenza dun compoñente de Debian non poderá restrinxir
     a ningunha das partes a venda ou dación do software como
     compoñente dunha distribución de agregación de software que conteña
     programas de diversas fontes. A licenza non
     deberá esixir compensación por dereitos de autor ou outras taxas por unha venta deste tipo.</p></li>
   <li><p><strong>Código Fonte</strong></p>
     <p>O aplicativo debe incluír o código fonte, e debe permitir
     a súa distribución, tanto como código fonte como en forma
     compilada.</p></li>
   <li><p><strong>Traballos Derivados</strong></p>
     <p>>A licenza debe permitir modificacións e traballos derivados, e
     debe permitir que sexan distribuídos baixo os mesmos termos
     da licenza do software orixinal.</p></li>
   <li><p><strong>Integridade do Código Fonte do Autor</strong></p>
     <p>A licenza poderá restrinxir a distribución do código fonte
     modificado <strong>só</strong> se a licenza permite
     a distribución de <q><tt>ficheiros de parches</tt></q> co código fonte
     para poder modificar o aplicativo á hora de compilalo.
     A licenza debe permitir explicitamente a distribución de
     software construído a partir de código fonte modificado. A licenza pode
     requirir que os traballos derivados leven un nome ou número
     de versión distintos ao do software orixinal.  (<em>Isto é un
     compromiso. O grupo Debian anima aos autores a non
     restrinxir a modificación de ningún ficheiro, sexa código
     fonte ou binario.</em>)</p></li>
   <li><p><strong>Non Discriminar a Persoas ou Grupos</strong></p>
     <p>A licenza non debe discriminar ningunha persoa ou
      grupos de persoas.</p></li>
   <li><p><strong>Non Discriminar por Área de Uso</strong></p>
     <p>A licenza non debe restrinxir a ninguén o uso do programa
     nunha área ou campo concretos. Por exemplo, non pode
     impedir que se use o programa nun negocio, ou
     para a investigación xenética.</p></li>
   <li><p><strong>Distribución da Licenza</strong></p>
     <p>Os dereitos asociados ao aplicativo deben aplicarse a todo
     aquel a quen se lle redistribúa, sen necesidade
     da execución de ningunha licenza adicional pola súa parte.</p></li>
   <li><p><strong>A Licenza Non Debe Ser Específica para Debian</strong></p>
     <p>Os dereitos asociados ao programa non poden depender de
     que este sexa parte do sistema Debian. Se o programa
     se extrae de Debian e se usa ou distribúe sen Debian,
     pero sempre dentro dos termos da licenza do programa, todas
     as partes ás que se redistribúa o aplicativo deben ter os
     mesmos dereitos que son concedidos en conxunción co
     sistema Debian.</p></li>
   <li><p><strong>A Licenza Non Debe Contaminar Outro Software</strong></p>
     <p>A licenza non debe engadirlle restricións a outro
     software que se distribúa xunto co software con
     licencia. Por exemplo, a licenza non debe insistir en que todos
     os outros programas cos que sexa distribuído no mesmo medio deban ser software
     libre.</p></li>
   <li><p><strong>Exemplos de Licenzas</strong></p>
     <p>As licenzas <q><strong><a href="https://www.gnu.org/copyleft/gpl.html">GPL</a></strong></q>,
     <q><strong><a href="https://opensource.org/licenses/BSD-3-Clause">BSD</a></strong></q>, e
     <q><strong><a href="https://perldoc.perl.org/perlartistic.html">Artistic</a></strong></q>
     son exemplos de licenzas que consideramos <q><em>libres</em></q>.</p></li>
</ol>

<p><em>O concepto de estabelecer o noso <q>contrato social coa comunidade de
software libre</q> foi suxerido por Ean Schuessler. Bruce Perens creou un
borrador deste documento, que foi revisado por outros desenvolvedores de
Debian nunha conferencia por correo electrónico que durou todo un mes, en
xuño de 1997, e despois foi
<a
href="https://lists.debian.org/debian-announce/debian-announce-1997/msg00017.html">\
aceptada</a> como a política publicamente estabelecida do proxecto
Debian.</em></p>

<p><em>Posteriormente Bruce Perens creou <a href="https://opensource.org/docs/definition.php"><q>A Definición do Software Aberto</q></a> eliminando as referencias 
exclusivas de Debian das Directrices de Software Libre de Debian.</em></p>

<p><em>Outras organizacións poden basearse neste documento. Se o fan, por
favor, fagan referencia ao proxecto Debian.</em></p>
