#use wml::debian::template title="Informations sur la publication &ldquo;Stretch&rdquo; de Debian"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="9f9207921c4ea799d5d3daa8a0f4b0faedf2b073" maintainer="Jean-Paul Guillonneau"

<p>La version de Debian <current_release_stretch> a été publiée le
<a href="$(HOME)/News/<current_release_newsurl_stretch/>"><current_release_date_stretch></a>.
<ifneq "9.0" "<current_release>"
  "La version 9.0 a été initialement publiée le <:=spokendate('2017-06-17'):>."
/>
Cette version comprend de nombreuses modifications décrites dans notre
<a href="$(HOME)/News/2017/20170617">communiqué de presse</a> et nos
<a href="releasenotes">notes de publication</a>.</p>

<p><strong>Debian 9 a été remplacée par
<a href="../buster/">Debian 10 (<q>Buster</q>)</a>.
La prise en charge des mises à jour de sécurité a cessé depuis le <:=spokendate('2020-07-06'):>.
</strong></p>

<p><strong>Stretch a bénéficié de la prise en charge à long terme (LTS) jusqu’à
la fin juin 2022. Cette prise en charge était limitée aux architectures i386,
amd64, armel, armhf et arm64.
Toutes les autres architectures n’étaient plus prises en charge dans Stretch.
Pour plus d’informations, veuillez consulter la <a
href="https://wiki.debian.org/LTS">section LTS du wiki de Debian</a>.
</strong></p>

<p>Pour obtenir et installer Debian, veuillez vous reporter à la page des
<a href="debian-installer/">informations d'installation</a> et au <a
href="installmanual">guide d'installation</a>. Pour mettre à niveau à
partir d'une ancienne version de Debian, veuillez vous reporter aux
instructions des <a href="releasenotes">notes de publication</a>.</p>

# Activate the following when LTS period starts.
<p>Les architectures suivantes étaient prises en charge par LTS :</p>

<ul>
<li><a href="../../ports/amd64/">PC 64 bits (amd64)</a>
<li><a href="../../ports/i386/">PC 32 bits (i386)</a>
<li><a href="../../ports/armel/">EABI ARM (armel)</a>
<li><a href="../../ports/armhf/">ARM avec unité de calcul flottant (armhf)</a>
<li><a href="../../ports/arm64/">ARM 64 bits (AArch64)</a>
</ul>

<p>Initialement lors de la publication de Stretch, les architectures suivantes
étaient prises en charge :</p>
<ul>
<li><a href="../../ports/amd64/">PC 64 bits (amd64)</a>
<li><a href="../../ports/i386/">PC 32 bits (i386)</a>
<li><a href="../../ports/armel/">EABI ARM (armel)</a>
<li><a href="../../ports/armhf/">ARM avec unité de calcul flottant (armhf)</a>
<li><a href="../../ports/mipsel/">MIPS (petit-boutiste)</a>
<li><a href="../../ports/mips/">MIPS (gros-boutiste)</a>
<li><a href="../../ports/s390x/">IBM System z</a>
<li><a href="../../ports/arm64/">ARM 64 bits (AArch64)</a>
<li><a href="../../ports/ppc64el/">Processeurs POWER</a>
<li><a href="../../ports/mips64el/">MIPS 64 bits (petit-boutiste)</a>
</ul>

<p>
Contrairement à nos souhaits, certains problèmes pourraient toujours exister
dans cette version, même si elle est déclarée <em>stable</em>. Nous avons
réalisé <a href="errata">une liste des principaux problèmes connus</a>, et vous
pouvez toujours nous <a href="../reportingbugs">signaler d'autres problèmes</a>.
</p>
