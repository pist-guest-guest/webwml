#use wml::debian::template title="Debian 12 – Errata" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="ba68acca0765fe2fd2a7fc056b6c929651151770" maintainer="Baptiste Jammet"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>


# <toc-add-entry name="known_probs">Problèmes connus</toc-add-entry>
<toc-add-entry name="security">Problèmes de sécurité</toc-add-entry>

<p>
L'équipe de sécurité Debian produit des mises à jour de paquets de la
version stable dans lesquels elle a identifié des problèmes de
sécurité. Merci de consulter les
<a href="$(HOME)/security/">pages concernant la sécurité</a> pour plus
d'informations concernant les problèmes de ce type identifiés dans 
<q>Bookworm</q>.
</p>

<p>
Si vous utilisez APT, ajoutez la ligne suivante à votre fichier
<tt>/etc/apt/sources.list</tt> pour récupérer les dernières mises à jour de
sécurité&nbsp;:
</p>

<pre>
  deb https://security.debian.org/ bookworm-security main contrib non-free non-free-firmware
</pre>

<p>
Ensuite, lancez <kbd>apt update</kbd> suivi de <kbd>apt upgrade</kbd>.
</p>


<toc-add-entry name="pointrelease">Les versions intermédiaires</toc-add-entry>

<p>
Dans le cas où il y a plusieurs problèmes critiques ou plusieurs mises
à jour de sécurité, la version de la distribution est parfois mise à jour.
Généralement, ces mises à jour sont indiquées comme étant des versions
intermédiaires.</p>

<ul>
  <li>La première version intermédiaire, 12.1, a été publiée le
      <a href="$(HOME)/News/2023/20230722">22 juillet 2023</a>.</li>
  <li>La deuxième version intermédiaire, 12.2, a été publiée le
      <a href="$(HOME)/News/2023/202310072">7 octobre 2023</a>.</li>
  <li>La troisième version intermédiaire, 12.3, a été reportée le
      <a href="$(HOME)/News/2023/2023120902">9 décembre 2023</a>.</li>
  <li>La quatrième version intermédiaire, 12.4, a été publiée le
      <a href="$(HOME)/News/2023/20231210">10 décembre 2023</a>.</li>
  <li>La cinquième version intermédiaire, 12.5, a été publiée le
      <a href="$(HOME)/News/2024/20240210">10 février 2024</a>.</li>
  <li>La sixième version intermédiaire, 12.6, a été publiée le
      <a href="$(HOME)/News/2024/20240210">29 juin 2024</a>.</li>
  <li>La septième version intermédiaire, 12.7, a été publiée le
      <a href="$(HOME)/News/2024/20240831">31 août 2024</a>.</li>
  <li>La huitième version intermédiaire, 12.8, a été publiée le
      <a href="$(HOME)/News/2024/20241109">9 novembre 2024</a>.</li>
</ul>

<ifeq <current_release_bookworm> 12.0 "

<p>
À l'heure actuelle, il n'y a pas de mise à jour pour Debian 12.
</p>" "

<p>
Veuillez consulter le <a
href=http://deb.debian.org/debian/dists/bookworm/ChangeLog>journal des
modifications</a> pour obtenir le détail des modifications entre la
version&nbsp;12.0 et la version&nbsp;<current_release_bookworm/>.
</p>"/>


<p>
Les corrections apportées à la version stable de la distribution passent
souvent par une période de test étendue avant d'être acceptées dans l'archive.
Cependant, ces corrections sont disponibles dans le répertoire <a
href="http://ftp.debian.org/debian/dists/bookworm-proposed-updates/">\
dists/bookworm-proposed-updates</a> de tout miroir de l'archive Debian.
</p>

<p>
Si vous utilisez APT pour mettre à jour vos paquets, vous pouvez installer les
mises à jour proposées en ajoutant la ligne suivante dans votre fichier
<tt>/etc/apt/sources.list</tt>&nbsp;:
</p>

<pre>
  \# Ajouts proposés pour une version intermédiaire 12
  deb https://deb.debian.org/debian bookworm-proposed-updates main contrib non-free-firmware non-free
</pre>

<p>Ensuite, exécutez <kbd>apt update</kbd> suivi de
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="installer">Système d'installation</toc-add-entry>

<p>
Pour plus d'informations à propos des errata et des mises à jour du système
d'installation, consultez la page de <a href="debian-installer/">l'installateur</a>.
</p>
