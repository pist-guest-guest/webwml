#use wml::debian::translation-check translation="9d41ab1625a3bbe9bf95b782d91e91b766a3f664" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de la mise à jour de Debian 11.9</define-tag>
<define-tag release_date>2024-02-10</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>11</define-tag>
<define-tag codename>Bullseye</define-tag>
<define-tag revision>11.9</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Le projet Debian a l'honneur d'annoncer la neuvième mise à jour de sa
distribution oldstable Debian <release> (nom de code <q><codename></q>).
Tout en réglant quelques problèmes importants, cette mise à jour corrige
principalement des problèmes de sécurité de la version stable. Les annonces de
sécurité ont déjà été publiées séparément et sont simplement référencées dans ce
document.
</p>

<p>
Veuillez noter que cette mise à jour ne constitue pas une nouvelle version de
Debian <release> mais seulement une mise à jour de certains des paquets qu'elle
contient. Il n'est pas nécessaire de jeter les anciens médias de la
version <codename>. Après installation, les paquets peuvent être mis à niveau
vers les versions actuelles en utilisant un miroir Debian à jour.
</p>

<p>
Les personnes qui installent fréquemment les mises à jour à partir de
security.debian.org n'auront pas beaucoup de paquets à mettre à jour et la
plupart des mises à jour de security.debian.org sont comprises dans cette mise
à jour.
</p>

<p>
De nouvelles images d'installation seront prochainement disponibles à leurs
emplacements habituels.
</p>

<p>
Mettre à jour une installation vers cette révision peut se faire en faisant
pointer le système de gestion de paquets sur l'un des nombreux miroirs HTTP de
Debian. Une liste complète des miroirs est disponible à l'adresse :
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>



<h2>Corrections de bogues divers</h2>

<p>
Cette mise à jour de la version oldstable apporte quelques corrections importantes
aux paquets suivants :
</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction axis "Filtrage des protocoles non pris en charge dans la classe client ServiceFactory [CVE-2023-40743]">
<correction base-files "Mise à jour pour cette version">
<correction cifs-utils "Correction des constructions non parallèles">
<correction compton "Retrait de la recommandation de picom">
<correction conda-package-handling "Tests non fiables ignorés">
<correction conmon "Pas de plantage lors de la transmission de conteneurs stdout/stderr avec beaucoup de sorties">
<correction crun "Correction des conteneurs avec systemd comme système d'initialisation lors de l'utilisation des versions les plus récentes du noyau">
<correction debian-installer "Passage de l'ABI du noyau Linux à la version 5.10.0-28 ; reconstruction avec proposed-updates">
<correction debian-installer-netboot-images "Reconstruction avec proposed-updates">
<correction debian-ports-archive-keyring "Ajout de la clé de signature automatique de l'archive des portages de Debian (2025)">
<correction debian-security-support "Marquage de tor, consul et xen comme en fin de vie ; prise en charge de samba limitée au cas d'utilisation sans contrôleur de domaine Active Directory ; correspondance avec des expressions rationnelles des paquets golang ; vérification de version supprimée ; ajout de chromium à security-support-ended.deb11 ; ajout de tiles et libspring-java à security-support-limited">
<correction debootstrap "Rétroportage des modifications de la prise en charge de merged-/usr à partir de Trixie : implémentation de merged-/usr par fusion post-installation, merged-/usr par défaut pour les suites plus récentes que Bookworm dans tous les profils">
<correction distro-info "Mise à jour des tests pour distro-info-data 0.58+deb12u1 ajustant la date de fin de vie de Debian 7">
<correction distro-info-data "Ajout d'Ubuntu 24.04 LTS Noble Numbat ; correction de plusieurs dates de fin de vie">
<correction dpdk "Nouvelle version amont stable">
<correction dropbear "Correction d'un problème de contournement de mesure de sécurité [CVE-2021-36369] ; correction de <q>l'attaque Terrapin</q> [CVE-2023-48795]">
<correction exuberant-ctags "Correction d'un problème d'exécution de commande arbitraire [CVE-2022-4515]">
<correction filezilla "Exploitation de l'attaque <q>Terrapin</q> évitée [CVE-2023-48795]">
<correction gimp "Retrait des anciennes version du greffon dds empaqueté séparément">
<correction glib2.0 "Alignement sur les corrections de la version amont stable ; corrections de problèmes de déni de service [CVE-2023-32665 CVE-2023-32611 CVE-2023-29499 CVE-2023-32636]">
<correction glibc "Correction d'une corruption de mémoire dans <q>qsort()</q> lors de l'utilisation de fonctions de comparaison non transitives.">
<correction gnutls28 "Correction de sécurité pour une attaque temporelle par canal auxiliaire [CVE-2023-5981]">
<correction imagemagick "Divers correctifs de sécurité [CVE-2021-20241 CVE-2021-20243 CVE-2021-20244 CVE-2021-20245 CVE-2021-20246 CVE-2021-20309 CVE-2021-3574 CVE-2021-39212 CVE-2021-4219 CVE-2022-1114 CVE-2022-28463 CVE-2022-32545 CVE-2022-32546]">
<correction jqueryui "Correction d'un problème de script intersite [CVE-2022-31160]">
<correction knewstuff "Assurance de ProvidersUrl correct pour corriger un déni de service">
<correction libdatetime-timezone-perl "Mise à jour des données de fuseau horaire incluses">
<correction libde265 "Correction d'un problème de violation de segmentation dans la fonction <q>decoder_context::process_slice_segment_header</q> [CVE-2023-27102] ; correction d'un problème de dépassement de tampon de tas dans la fonction <q>derive_collocated_motion_vectors</q> [CVE-2023-27103] ; correction d'un problème de dépassement de tampon en lecture dans <q>pic_parameter_set::dump</q> [CVE-2023-43887] ; correction d'un problème de dépassement de tampon dans la fonction <q>slice_segment_header</q> [CVE-2023-47471] ; correction de problèmes de dépassement de tampon [CVE-2023-49465 CVE-2023-49467 CVE-2023-49468]">
<correction libmateweather "Mise à jour des données de localisation incluses ; mise à jour de l'URL du serveur de données">
<correction libpod "Correction de la gestion incorrecte de groupes supplémentaires [CVE-2022-2989]">
<correction libsolv "Activation de la prise en charge de la compression zstd">
<correction libspreadsheet-parsexlsx-perl "Correction d'une possible <q>memory bomb</q> [CVE-2024-22368] ; correction d'un problème d'entité externe XML [CVE-2024-23525]">
<correction linux "Nouvelle version amont stable ; passage de l'ABI à la version 28">
<correction linux-signed-amd64 "Nouvelle version amont stable ; passage de l'ABI à la version 28">
<correction linux-signed-arm64 "Nouvelle version amont stable ; passage de l'ABI à la version 28">
<correction linux-signed-i386 "Nouvelle version amont stable ; passage de l'ABI à la version 28">
<correction llvm-toolchain-16 "Nouveau paquet rétroporté pour prendre en charge les constructions des nouvelles versions de chromium ; dépendance de construction à <q>llvm-spirv</q> plutôt qu'à <q>llvm-spirv-16</q>">
<correction mariadb-10.5 "Nouvelle version amont stable ; correction d'un problème de déni de service [CVE-2023-22084]">
<correction minizip "Rejet des dépassements des champs d'en-tête zip [CVE-2023-45853]">
<correction modsecurity-apache "Correction de problèmes de contournement de protection [CVE-2022-48279 CVE-2023-24021]">
<correction nftables "Correction d'un problème de génération incorrecte de bytecode">
<correction node-dottie "Correction d'un problème de pollution de prototype [CVE-2023-26132]">
<correction node-url-parse "Correction d'un problème de contournement d'autorisation [CVE-2022-0512]">
<correction node-xml2js "Correction d'un problème de pollution de prototype [CVE-2023-0842]">
<correction nvidia-graphics-drivers "Nouvelle version amont [CVE-2023-31022]">
<correction nvidia-graphics-drivers-tesla-470 "Nouvelle version amont [CVE-2023-31022]">
<correction opendkim "Suppression correcte des en-têtes Authentication-Results [CVE-2022-48521]">
<correction perl "Dépassement de tampon au moyen d'une propriété Unicode interdite évité [CVE-2023-47038]">
<correction plasma-desktop "Correction d'un bogue de déni de service dans discover">
<correction plasma-discover "Correction d'un bogue de déni de service ; correction d'un échec de construction">
<correction postfix "Nouvelle version amont stable ; traitement d'un problème de dissimulation d'adresse SMTP [CVE-2023-51764]">
<correction postgresql-13 "Nouvelle version amont stable ; correction d'un problème d'injection de code SQL [CVE-2023-39417]">
<correction postgresql-common "Correction d'autopkgtests">
<correction python-cogent "Tests parallèles évités sur les systèmes avec un seul processeur">
<correction python-django-imagekit "Déclenchement de détection de traversée de répertoires évité dans les tests">
<correction python-websockets "Correction d'un problème de durée prédictible [CVE-2021-33880]">
<correction pyzoltan "Prise en charge de la construction sur les systèmes avec un seul cœur">
<correction ruby-aws-sdk-core "Inclusion d'un fichier VERSION dans le paquet">
<correction spip "Correction d'un problème de script intersite">
<correction swupdate "Évitement de l’acquisition de privilèges de superutilisateur au moyen d'un mode de socket inapproprié">
<correction symfony "Assurance que les filtres de CodeExtension échappent correctement leurs entrées [CVE-2023-46734]">
<correction tar "Correction de vérification de limites dans le décodeur base 256 [CVE-2022-48303], de la gestion des préfixes d'en-tête étendus [CVE-2023-39804]">
<correction tinyxml "Correction d'un problème d'assertion [CVE-2023-34194]">
<correction tzdata "Mise à jour des données de zone horaire incluses">
<correction unadf "Correction d'un problème de dépassement de tampon de pile [CVE-2016-1243] ; correction d'un problème d'exécution de code arbitraire [CVE-2016-1244]">
<correction usb.ids "Mise à jour de la liste des données incluses">
<correction vlfeat "Correction de l'échec de construction à partir des sources avec les versions récentes d'ImageMagick">
<correction weborf "Correction d'un problème de déni de service">
<correction wolfssl "Correction de problèmes de dépassement de tampon [CVE-2022-39173 CVE-2022-42905], d'un problème de divulgation de clé [CVE-2022-42961], d'un problème de tampon prédictible dans les informations de saisie d'entrée (IKM) [CVE-2023-3724]">
<correction xerces-c "Correction d'un problème d'utilisation de mémoire après libération [CVE-2018-1311] ; correction d'un problème de dépassement d'entier [CVE-2023-37536]">
<correction zeromq3 "Correction de la détection de <q>fork()</q> avec gcc 7 ; mise à jour de la déclaration de modification de licence de copyright">
</table>


<h2>Mises à jour de sécurité</h2>


<p>
Cette révision ajoute les mises à jour de sécurité suivantes à la version
oldstable. L'équipe de sécurité a déjà publié une annonce pour chacune de ces
mises à jour :
</p>

<table border=0>
<tr><th>Identifiant</th>  <th>Paquet</th></tr>
<dsa 2023 5496 firefox-esr>
<dsa 2023 5499 chromium>
<dsa 2023 5506 firefox-esr>
<dsa 2023 5508 chromium>
<dsa 2023 5509 firefox-esr>
<dsa 2023 5511 mosquitto>
<dsa 2023 5512 exim4>
<dsa 2023 5513 thunderbird>
<dsa 2023 5514 glibc>
<dsa 2023 5515 chromium>
<dsa 2023 5516 libxpm>
<dsa 2023 5517 libx11>
<dsa 2023 5518 libvpx>
<dsa 2023 5519 grub-efi-amd64-signed>
<dsa 2023 5519 grub-efi-arm64-signed>
<dsa 2023 5519 grub-efi-ia32-signed>
<dsa 2023 5519 grub2>
<dsa 2023 5520 mediawiki>
<dsa 2023 5522 tomcat9>
<dsa 2023 5523 curl>
<dsa 2023 5524 libcue>
<dsa 2023 5526 chromium>
<dsa 2023 5527 webkit2gtk>
<dsa 2023 5528 node-babel7>
<dsa 2023 5530 ruby-rack>
<dsa 2023 5531 roundcube>
<dsa 2023 5533 gst-plugins-bad1.0>
<dsa 2023 5534 xorg-server>
<dsa 2023 5535 firefox-esr>
<dsa 2023 5536 chromium>
<dsa 2023 5537 openjdk-11>
<dsa 2023 5538 thunderbird>
<dsa 2023 5539 node-browserify-sign>
<dsa 2023 5540 jetty9>
<dsa 2023 5542 request-tracker4>
<dsa 2023 5543 open-vm-tools>
<dsa 2023 5544 zookeeper>
<dsa 2023 5545 vlc>
<dsa 2023 5546 chromium>
<dsa 2023 5547 pmix>
<dsa 2023 5548 openjdk-17>
<dsa 2023 5549 trafficserver>
<dsa 2023 5550 cacti>
<dsa 2023 5551 chromium>
<dsa 2023 5554 postgresql-13>
<dsa 2023 5556 chromium>
<dsa 2023 5557 webkit2gtk>
<dsa 2023 5558 netty>
<dsa 2023 5560 strongswan>
<dsa 2023 5561 firefox-esr>
<dsa 2023 5563 intel-microcode>
<dsa 2023 5564 gimp>
<dsa 2023 5565 gst-plugins-bad1.0>
<dsa 2023 5566 thunderbird>
<dsa 2023 5567 tiff>
<dsa 2023 5569 chromium>
<dsa 2023 5570 nghttp2>
<dsa 2023 5571 rabbitmq-server>
<dsa 2023 5572 roundcube>
<dsa 2023 5573 chromium>
<dsa 2023 5574 libreoffice>
<dsa 2023 5576 xorg-server>
<dsa 2023 5577 chromium>
<dsa 2023 5579 freeimage>
<dsa 2023 5581 firefox-esr>
<dsa 2023 5582 thunderbird>
<dsa 2023 5584 bluez>
<dsa 2023 5585 chromium>
<dsa 2023 5586 openssh>
<dsa 2023 5587 curl>
<dsa 2023 5588 putty>
<dsa 2023 5590 haproxy>
<dsa 2023 5591 libssh>
<dsa 2023 5592 libspreadsheet-parseexcel-perl>
<dsa 2024 5594 linux-signed-amd64>
<dsa 2024 5594 linux-signed-arm64>
<dsa 2024 5594 linux-signed-i386>
<dsa 2024 5594 linux>
<dsa 2024 5595 chromium>
<dsa 2024 5597 exim4>
<dsa 2024 5598 chromium>
<dsa 2024 5599 phpseclib>
<dsa 2024 5600 php-phpseclib>
<dsa 2024 5602 chromium>
<dsa 2024 5603 xorg-server>
<dsa 2024 5604 openjdk-11>
<dsa 2024 5605 thunderbird>
<dsa 2024 5606 firefox-esr>
<dsa 2024 5608 gst-plugins-bad1.0>
<dsa 2024 5613 openjdk-17>
<dsa 2024 5614 zbar>
<dsa 2024 5615 runc>
</table>


<h2>Paquets supprimés</h2>

<p>Le paquet suivant obsolète a été supprimé de la distribution:</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction gimp-dds "Intégré dans gimp >=2.10">

</table>

<h2>Installateur Debian</h2>
<p>
L'installateur a été mis à jour pour inclure les correctifs incorporés dans
cette version d'oldstable.
</p>

<h2>URL</h2>

<p>
Liste complète des paquets qui ont été modifiés dans cette version :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>
Adresse de l'actuelle distribution oldstable :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable/">
</div>

<p>Mises à jour proposées à la distribution oldstable :</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>
Informations sur la distribution oldstable (notes de publication,
<i>errata</i>, etc.) :
</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>
Annonces et informations de sécurité :
</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>À propos de Debian</h2>
<p>
Le projet Debian est une association de développeurs de logiciels libres qui
offrent volontairement leur temps et leurs efforts pour produire le système
d'exploitation complètement libre Debian.
</p>

<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier
électronique à &lt;press@debian.org&gt; ou contactez l'équipe de publication de
la version stable à &lt;debian-release@lists.debian.org&gt;.
</p>

