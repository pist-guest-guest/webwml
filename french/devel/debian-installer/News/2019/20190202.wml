#use wml::debian::translation-check translation="382d74dd180560f497ab7c7211b0d47b51988eae" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de l'installateur Debian Buster Alpha 5</define-tag>
<define-tag release_date>2019-02-02</define-tag>
#use wml::debian::news

<p>
L'<a href="https://wiki.debian.org/DebianInstaller/Team">équipe</a> du
programme d'installation de Debian a le plaisir d'annoncer la parution
de la cinquième version alpha pour Debian 10 <q>Buster</q>.
</p>


<h2>Modifications importantes dans cette version de l'installateur</h2>

<p>L'équipe du programme d'installation de Debian est heureuse d'annoncer
que la cinquième version alpha pour Debian Buster de l'installateur inclut
une prise en charge initiale du démarrage sécurisé d'UEFI (« Secure Boot »
– SB) dans les supports d'installation de Debian.</p>

<p>Cette prise en charge n'est pas encore complète et nous souhaiterions
solliciter votre aide ! Un article détaillé avec davantage d'information sur
le démarrage sécurisé et avec un lien vers les instructions sur les moyens
d'effectuer des tests et de rendre compte de vos découvertes
<a href="https://bits.debian.org/2019/02/testing-initial-secure-boot-support.html">a été
publié séparément</a>.</p>


<h2>Améliorations dans cette version de l'installateur</h2>

<ul>
  <li>btrfs-progs:
    <ul>
      <li>ajout de la prise en charge de libzstd.</li>
    </ul>
  </li>
  <li>cdebconf :
    <ul>
      <li>notification à gtk-font-set du facteur de zoom.</li>
    </ul>
  </li>
  <li>debian-installer :
    <ul>
      <li>passage de l'ABI du noyau Linux de 4.18.0-3 à 4.19.0-1.</li>
    </ul>
  </li>
  <li>debootstrap :
    <ul>
      <li>ajout d'optimisations au moment de l'exécution (<a href="https://bugs.debian.org/871835">nº 871835</a>).</li>
    </ul>
  </li>
  <li>fonts-noto :
    <ul>
      <li>correction de la prise en charge du gujarati par l'inclusion de
        Noto Serif Gujarati dans le paquet udeb principal (<a href="https://bugs.debian.org/911705">nº 911705</a>,
        <a href="https://bugs.debian.org/915825">nº 915825</a>).</li>
    </ul>
  </li>
  <li>grub-installer :
    <ul>
      <li>ajout de la prise en charge de shim-signed et de
        grub-efi-amd64-signed, nécessaires par défaut pour l'installation de
        paquets signés sur amd64.</li>
    </ul>
  </li>
  <li>grub2 :
    <ul>
      <li>ajout des modules LUKS aux images UEFI signées (<a href="https://bugs.debian.org/908162">nº 908162</a>) ;</li>
      <li>rétroportage de la prise en charge du client Xen PVH à partir de
        l'amont (<a href="https://bugs.debian.org/776450">nº 776450</a>) ;</li>
      <li>amélioration de la gestion d'initrd arm/arm64 (<a href="https://bugs.debian.org/907596">nº 907596</a>,
        <a href="https://bugs.debian.org/909420">nº 909420</a>,
        <a href="https://bugs.debian.org/915091">nº 915091</a>) ;</li>
      <li>pas d'activation de la validation de signature Shim si le
        démarrage sécurisé est désactivé.</li>
    </ul>
  </li>
  <li>localechooser :
    <ul>
      <li>ajout de la chaîne en anglais manquante « Choose language » à
        certaines traductions : bosniaque, hongrois, tadjik.</li>
    </ul>
  </li>
  <li>partconf :
    <ul>
      <li>suppression de la prise en charge de reiserfs (<a href="https://bugs.debian.org/717534">nº 717534</a>).</li>
    </ul>
  </li>
  <li>partman-lvm :
    <ul>
      <li>correction des caractères invalides dans les noms de groupes de
        volumes (<a href="https://bugs.debian.org/911036">nº 911036</a>).</li>
    </ul>
  </li>
  <li>partman-md :
    <ul>
      <li>lors de la création d'un nouvel ensemble RAID1/5/6/10, démarrage
        de la synchronisation à la vitesse minimale permise par le système
        plutôt que de la laisser fonctionner à pleine vitesse, ralentissant
        l'installation de paquets (<a href="https://bugs.debian.org/838503">nº 838503</a>).</li>
    </ul>
  </li>
  <li>rootskel-gtk :
    <ul>
      <li>utilisation de la fonte Noto Serif Gujarati pour le gujarati (<a
        href="https://bugs.debian.org/911705">nº 911705</a>,
        <a href="https://bugs.debian.org/915825">nº 915825</a>) ;</li>
      <li>application du zoom de caractères défini par le raccourci de
        cdebconf.</li>
    </ul>
  </li>
  <li>win32-loader :
    <ul>
      <li>inclusion du trousseau de clés debian-archive-removed-keys pour
        assurer que la vérification de signature est effectuée avec succès.</li>
    </ul>
  </li>
  <li>wpa :
    <ul>
      <li>réactivation de TLSv1.0 et du niveau de sécurité 1 pour
        wpasupplicant (<a href="https://bugs.debian.org/907518">nº 907518</a>, <a href="https://bugs.debian.org/911297">nº 911297</a>).</li>
    </ul>
  </li>
</ul>


<h2>Modifications de la prise en charge matérielle</h2>

<ul>
  <li>debian-installer :
    <ul>
      <li>[arm64] utilisation d'arm-trusted-firmware à la place
        d'atf-allwinner pour construire les images pine64_plus et pinebook ;</li>
      <li>[armhf] réactivation de l'image Firefly-RK3288 ;</li>
      <li>[armhf] mise à jour des dépendances de construction pour
        u-boot-rockchip.</li>
    </ul>
  </li>
  <li>flash-kernel :
    <ul>
      <li>ajout d'une entrée dans la base de données de matériel pour Odroid
        HC1 (<a href="https://bugs.debian.org/916980">nº 916980</a>) ;</li>
      <li>ajout d'une entrée dans la base de données de matériel pour
        Seagate Blackarmor NAS220 (<a href="https://bugs.debian.org/918193">nº 918193</a>).</li>
    </ul>
  </li>
  <li>linux :
    <ul>
      <li>udeb : définition du paquet mtd-core-modules pour contenir le
        noyau MTD s'il n'est pas intégré ;</li>
      <li>udeb : déplacement du noyau MTD de nic-modules à mtd-core-modules.</li>
    </ul>
  </li>
  <li>partman-partitioning :
    <ul>
      <li>définition du type de label de disque par défaut pour les systèmes
        basés sur RISC-V à gpt.</li>
    </ul>
  </li>
</ul>


<h2>État de la localisation</h2>

<ul>
  <li>76 langues sont prises en charge dans cette version.</li>
  <li>La traduction est complète pour 31 de ces langues.</li>
</ul>


<h2>Problèmes connus dans cette version</h2>

<p>
Veuillez consulter les <a href="$(DEVEL)/debian-installer/errata">errata</a>
pour plus de détails et une liste complète des problèmes connus.
</p>


<h2>Retours d'expérience pour cette version</h2>

<p>
Nous avons besoin de votre aide pour trouver des bogues et améliorer encore
l'installateur, merci de l'essayer. Les CD, les autres supports d'installation,
et tout ce dont vous pouvez avoir besoin sont disponibles sur notre
<a href="$(DEVEL)/debian-installer">site web</a>.
</p>


<h2>Remerciements</h2>

<p>
L'équipe du programme d'installation Debian remercie toutes les personnes ayant
pris part à cette publication.
</p>
