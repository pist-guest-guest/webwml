#use wml::debian::cdimage title="Télécharger les images de USB/CD/DVD de Debian par HTTP ou par FTP" BARETITLE=true
#use wml::debian::translation-check translation="019351f156c8f8ffc0f7922929f7062fb4b732db" maintainer="Jean-Paul Guillonneau"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"
# Translators:
# Willy Picard, 2002-2004.
# Nicolas Bertolissio, 2004.
# Thomas Huriaux, 2004-2006.
# Frédéric Bothamy, 2007.
# Simon Paillard, 2007, 2009.
# Thomas Péteul, 2009, 2010.
# David Prévot, 2011.
# Baptiste Jammet, 2015-2016.

<p>Les images suivantes de Debian peuvent être téléchargées&nbsp;:</p>

<ul>

  <li><a href="#stable">images officielles de USB/CD/DVD de
  la distribution <q>stable</q></a>&nbsp;;</li>

  <li><a href="https://cdimage.debian.org/cdimage/weekly-builds/">images
  officielles de USB/CD/DVD de la distribution <q>testing</q> (<em>régénérées
  une fois par semaine</em>)</a>.</li>

</ul>

<p>Voir également :</p>
<ul>

  <li>la <a href="#mirrors">liste complète des miroirs <tt>debian-cd/</tt></a> ;</li>

  <li>la page <a href="../netinst/">Installation par le réseau</a> pour des
  images <q>d’installation par le réseau</q> ;</li>

  <li>la page <a href="$(DEVEL)/debian-installer/">Installation avec
  l’installateur Debian</a> pour des images de la distribution <q>testing</q>.</li>

</ul>

<hr/>

<h2><a name="stable">Images officielles de USB/CD/DVD de la
distribution <q>stable</q></a></h2>

<p>Pour installer Debian sur une machine sans connexion Internet, il est
possible d'utiliser des images de CD/USB (700&nbsp;Mo chacune) ou des
images de DVD/USB (4,4&nbsp;Go chacune). Téléchargez le fichier de
l'image du premier CD/USB ou DVD/USB, gravez-le en utilisant un
logiciel de gravure de USB/CD/DVD, puis redémarrez à partir de ce média.</p>

<p>Le <strong>premier</strong> disque USB/CD/DVD) contient
tous les fichiers nécessaires pour installer un système Debian
standard.<br/>
</p>

<div class="line">
<div class="item col50">
<p><strong>CD/USB</strong></p>

<p>Les liens suivants pointent vers des fichiers image d'une taille
n'excédant pas 700&nbsp;Mo afin qu'ils puissent être gravés sur des
médias CD-R(W)&nbsp;:</p>

<stable-full-cd-images />
</div>
<div class="item col50 lastcol">
<p><strong>DVD/USB</strong></p>

<p>Les liens suivants pointent vers des fichiers image d'une taille
n'excédant pas 4,7&nbsp;Go afin qu'ils puissent être gravés sur des
disques DVD-R/DVD+R ou des médias similaires&nbsp;:</p>

<stable-full-dvd-images />
</div><div class="clear"></div>
</div>

<p>Assurez-vous de lire la documentation avant de faire l'installation.
<strong>Si vous ne devez lire qu'un seul document</strong> avant
l'installation, veuillez lire notre
<a href="$(HOME)/releases/stable/amd64/apa">guide d'installation</a>, un
parcours rapide du processus d'installation. D'autres documentations
utiles&nbsp;:</p>
<ul>
<li><a href="$(HOME)/releases/stable/installmanual">le manuel d'installation</a>,
    les instructions détaillées d'installation&nbsp;;</li>
<li><a href="https://wiki.debian.org/DebianInstaller">la documentation de
    l'installateur Debian</a>, y compris la FAQ avec des questions et
    réponses récurrentes&nbsp;;</li>
<li><a href="$(HOME)/releases/stable/debian-installer/#errata">les
    errata de l'installateur Debian</a>, la liste des problèmes connus
    dans l'installateur.</li>
</ul>

<hr />

<h2><a name="mirrors">Miroirs répertoriés de l'archive <q>debian-cd</q></a></h2>

<p>Veuillez noter que <strong>certains miroirs peuvent ne pas être à jour</strong>
&mdash;&nbsp;la publication « stable » actuelle des images de USB/CD/DVD est
<strong><current-cd-release></strong>.


<p><strong>En cas de doute, utilisez le
<a href="https://cdimage.debian.org/debian-cd/">serveur principal des images
de CD</a> en Suède</strong></p>.

<p>Souhaitez-vous proposer les images de CD de Debian sur votre
miroir&nbsp;? Si oui, veuillez consulter les
<a href="../mirroring/">instructions concernant la mise en place d'un
miroir d'images de CD</a>.</p>

#use wml::debian::countries
#include "$(ENGLISHDIR)/CD/http-ftp/cdimage_mirrors.list"
