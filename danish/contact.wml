#use wml::debian::template title="Hvordan man kontakter os" NOCOMMENTS="yes" MAINPAGE="true"
#use wml::debian::translation-check translation="fb050641b19d0940223934350c51061fcb2439a9"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
    <li><a href="#generalinfo">Generelle oplysninger</a>
    <li><a href="#installuse">Installering og brug af Debian</a>
    <li><a href="#press">Markedsføring og presse</a>
    <li><a href="#events">Begivenheder og konferencer</a>
    <li><a href="#helping">Hjælp Debian</a>
    <li><a href="#packageproblems">Rapportering af problemer i Debian-pakker</a>
    <li><a href="#development">Debian-udvikling</a>
    <li><a href="#infrastructure">Problemer med Debians infrastruktur</a>
    <li><a href="#harassment">Problemer med chikane</a>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Vi beder om at indledende 
forespørgsler sendes på <strong>engelsk</strong>, da de fleste af os forstår 
sproget.  Hvis det ikke er muligt, så bed om hjælp på en af vores 
<a href="https://lists.debian.org/users.html#debian-user">brugerpostlister</a>, 
som er tilgængelige på mange forskellige sprog.</p>
</aside>

<p>Debian er en stor organisation og der er flere måder at kontakte 
projektmedlemmer og andre Debian-brugere.  Denne side giver et overblik over 
hyppigt efterspurgte kontaktmetoder; siden er på ingen måde udtømmende.  Se de 
øvrige websider for andre kontaktmetoder.</p>

<p>Bemærk at de fleste e-mail-adresser angivet herunder er åbne postlister med
offentlige arkiver.  Læs <a href="$(HOME)/MailingLists/disclaimer">\
ansvarsfralæggelsen</a> før du sender en meddelelse.</p>


<h2 id="generalinfo">Generelle oplysninger</h2>

<p>De fleste oplysninger om Debian er samlet på vores websted,
<a href="$(HOME)">https://www.debian.org/</a>, kig på og 
<a href="$(SEARCH)">gennemsøg</a> det, før du kontakter os.</p>

<p>Du kan sende spørgsmål angående Debian-projektet til postlisten 
<email debian-project@lists.debian.org>.  Send ikke generelle spørgsmål om Linux 
til listen; læs videre for flere oplysninger om andre postlister.</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="doc/user-manuals#faq">Læs vores OSS</a></button></p>


<aside class="light">
  <span class="fa fa-envelope fa-5x"></span>
</aside>
<h2 id="installuse">Installering og brug af Debian</h2>

<p>Hvis du er sikker på, at dokumentationen på installeringsmediet og på vores 
websted ikke har en løsning på dit problem, kan du anvende den meget aktive 
brugerpostliste, hvor Debian-brugere og -udviklere kan besvare dine spørgsmål.
Her kan du stille spørgsmål om:</p>

<ul>
    <li>Installering</li>
    <li>Konfigurering</li>
    <li>Understøttet hardware</li>
    <li>Computeradministration</li>
    <li>Brug af Debian</li>
</ul>

<p><a href="https://lists.debian.org/debian-user/">Tegn abonnement</a> på listen 
og send dine spørgsmål til <email debian-user@lists.debian.org>.</p>

<p>Desuden er der brugerpostlister på forskellige sprog.  Se 
abonnementsoplysningerne vedrørende de 
<a href="https://lists.debian.org/users.html#debian-user">internationale 
postlister</a>.</p>

<p>Du kan gennemse vores <a href="https://lists.debian.org/">postlistearkiv</a> 
eller <a href="https://lists.debian.org/search.html">søge</a> i arkiverene, 
uden at det er nødvendigt at tegne abonnement.</p>

<p>Derudover er det muligt at gennemse vores postlister som nyhedsgrupper.</p>

<p>Hvis du mener at have fundet en fejl i vores installeringssystem, så skriv 
til postlisten <email debian-boot@lists.debian.org>.  Du kan også indsende en 
<a href="$(HOME)/releases/stable/i386/ch05s04#submit-bug">fejlrapport</a> mod 
pseudopakken <a href="https://bugs.debian.org/debian-installer">\
debian-installer</a>.</p>


<aside class="light">
  <span class="far fa-newspaper fa-5x"></span>
</aside>
<h2 id="press">Markedsføring og presse</h2>

<p><a href="https://wiki.debian.org/Teams/Publicity">Debian Publicity-holdet</a> 
udgiver nyheder og annonceringer på alle Debians officielle ressourcer, for 
eksempel nogle postlister, bloggen, mikronyhedswebstedet og officielle kanaler 
på sociale medier.</p>

<p>Hvis du skriver om Debian og har brug for hjælp eller oplysninger, så 
kontakt vores <a href="mailto:press@debian.org">PR-hold</a>.  Det er også den 
rigtige adresse, hvis du har i sinde at indsende nyheder til vores egen 
nyhedsside.</p>


<aside class="light">
  <span class="fas fa-handshake fa-5x"></span>
</aside>
<h2 id="events">Begivenheder og konferencer</h2>

<p>Send invitationer vedrørende <a href="$(HOME)/events/">konferencer</a>, 
udstillinger eller andre begivenheder til <email events@debian.org>.</p>

<p>Forespørgler om foldere, plakater og deltagelse i Europa, skal sendes til 
postlisten <email debian-events-eu@lists.debian.org>.</p>


<aside class="light">
  <span class="fas fa-hands-helping fa-5x"></span>
</aside>
<h2 id="helping">Hjælp Debian</h2>

<p>Der er mange måder at støtte Debian-projektet på, samt bidrage til 
distributionen.  Hvis du vil tilbyde din hjælp, så besøg først vores side om 
<a href="devel/join/">hvordan man deltager i Debian</a>.</p>

<p>Hvis du ønsker at vedligeholde et Debian-filspejl (mirror), så se siderne om 
<a href="mirror/">spejling af Debian</a>.  Oplysninger om nye filspejle 
indsendes via <a href="mirror/submit">formularen</a> hertil.  Problemer med 
eksisterende filspejle kan rapporteres til <email mirrors@debian.org>.</p>

<p>Hvis du ønsker at sælge Debian-cd'er/-dvd'er, så se <a href="CD/vendors/info">\
oplysninger til cd-forhandlere</a>.  For at komme på cd-forhandlerlisten bedes
du anvende <a href="CD/vendors/adding-form">formularen</a> hertil.</p>


<aside class="light">
  <span class="fas fa-bug fa-5x"></span>
</aside>
<h2 id="packageproblems">Rapportering af problemer i Debian-pakker</h2>

<p>Hvis du ønsker at indsende en fejlrapport om en Debian-pakke, har vi et 
fejlrapporteringssystem, hvor du let kan rapportere om problemet. Læs
<a href="Bugs/Reporting">instruktioner om indsendelse af fejlrapporter</a>.</p>

<p>Ønsker du blot at kommunikere med vedligeholderen af en Debian-pakke, kan du
anvende de særlige postaliaser oprettet for hver pakke.  Al post der sendes til 
&lt;<var>pakkenavn</var>&gt;@packages.debian.org, vil blive videresendt til 
den vedligeholder, som er ansvarlig for pakken.</p>

<p>Hvis du vil gøre Debian-udviklerne opmærksomme på et sikkerhedsproblem på en
diskret måde, send en mail til <email security@debian.org>.</p>


<aside class="light">
  <span class="fas fa-code-branch fa-5x"></span>
</aside>
<h2 id="development">Debian-udvikling</h2>

<p>Har du spørgsmål om udviklingen af Debian, har vi flere
<a href="https://lists.debian.org/devel.html">postlister</a> i Debian til det 
formål.  Her kan du komme i kontakt med vores udviklere.</p>

<p>Der er også en generel postliste for udviklere:
<email debian-devel@lists.debian.org>.  Følg dette dette
<a href="https://lists.debian.org/debian-devel/">link</a> for at tegne 
abonnement på den.</p>


<aside class="light">
  <span class="fas fa-network-wired fa-5x"></span>
</aside>
<h2 id="infrastructure">Problemer med Debians infrastruktur</h2>

<p>For at rapportere problemer med en Debian-tjenste, kan du normalt
<a href="Bugs/Reporting">rapportere en fejl</a> mod en passende
<a href="Bugs/pseudo-packages">pseudopakke</a>.</p>

<p>Alternativt kan du sende en mail til en af følgende adresser:</p>

<define-tag btsurl>pakke: <a href="https://bugs.debian.org/%0">%0</a></define-tag>

<dl>
<dt>Websidernes redaktører</dt>
  <dd><btsurl www.debian.org><br />
    <email debian-www@lists.debian.org></dd>
#include "$(ENGLISHDIR)/devel/website/tc.data"
<ifneq "$(CUR_LANG)" "English" "
<dt>Oversætter(e) til dansk af websiderne</dt>
<dd><: &list_translators($CUR_LANG); :></dd>
">
<dt>Postlistearkivernes administratorer og vedligeholdere</dt>
  <dd><btsurl lists.debian.org><br />
    <email listmaster@lists.debian.org></dd>
<dt>Fejlrapporteringssystemets administratorer</dt>
  <dd><btsurl bugs.debian.org><br />
    <email owner@bugs.debian.org></dd>
</dl>


<aside class="light">
  <span class="fas fa-umbrella fa-5x"></span>
</aside>
<h2 id="harassment">Problemer med chikane</h2>

<p>Vi har også en komplet liste over forskellige <a href="intro/organization">\
roller og mailadresser</a>, som kan anvendes til at kontakte organisationens
forskellige dele.</p>
