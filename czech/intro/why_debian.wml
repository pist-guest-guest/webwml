#use wml::debian::template title="Proč používat Debian" MAINPAGE="true"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="5eef343480ec39f5bb8db5e4e69bbb9e8b926c9b" maintainer="Vratislav Hutsky"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#users">Debian pro uživatele</a></li>
    <li><a href="#devel">Debian pro vývojáře</a></li>
    <li><a href="#enterprise">Debian v podnikovém prostředí</a></li>
  </ul>
</div>

<p>
Je spousta důvodů pro to, vybrat si Debian jako svůj operační systém - ať
už jste uživatel, vývojář nebo i za účelem podnikového nasazení.
Většina uživatelů ocení stabilitu a bezproblémový průběh aktualizací
jak jednotlivých balíčků, tak i celého systému. Debian také často
využívají vývojáři softwaru a výrobci hardwaru, neboť funguje na mnoha různých
architekturách a zařízeních, nabízí veřejný systém pro sledování chyb
a různé další nástroje pro vývojáře. Pokud máte v plánu používat Debian
k pracovním účelům, nabízí se i další výhody jako verze s prodlouženou
podporou nebo obrazy v cloudu.
</p>

<aside>
<p><span class="far fa-comment-dots fa-3x"></span> Pro mě se jedná o ideální kombinaci uživatelské přívětivosti a stability. Během let jsem vyzkoušel různé distribuce, ale jedině Debian prostě funguje. <a href="https://www.reddit.com/r/debian/comments/8r6d0o/why_debian/e0osmxx">NorhamsFinest na Redditu</a></p>
</aside>

<h2><a id="users">Debian pro uživatele</a></h2>

<dl>
  <dt><strong>Debian je svobodný software.</strong></dt>
  <dd>
    Debian obsahuje svobodný a otevřený software a
    <a href="free">svobodným zůstane</a> i v budoucnu. Kdokoli jej může používat,
    upravovat a dále kopírovat. Toto je zásadní slib, který
    <a href="../users">našim uživatelům</a> dáváme. A navíc je zcela zdarma.
  </dd>
</dl>

<dl>
  <dt><strong>Debian je stabilní a bezpečný.</strong></dt>
  <dd>
    Debian je operační systém založený na linuxovém jádře s podporou širokoé
    škály zařízení včetně notebooků, stolních PC i serverů. U všech balíčů
    nabízíme rozumné výchozí nastavení a pravidelné bezpečnostní záplaty
    po celou dobu jejich podpory.
  </dd>
</dl>

<dl>
  <dt><strong>Debian podporuje velké množství hardwaru.</strong></dt>
  <dd>
    Většina hardwaru, který je podporován v linuxovém jádře, bude podporovaný
    také v Debianu. Pokud je to někde potřeba, jsou k dispozici
    i proprietární ovladače.
  </dd>
</dl>

<dl>
  <dt><strong>Debian nabízí flexibilní instalátor.</strong></dt>
  <dd>
    Naše <a
    href="https://cdimage.debian.org/images/release/current-live/amd64/iso-hybrid/">živé
    obrazy</a> se hodí komukoliv, kdo by si Debian chtěl vyzkoušet před tím, než si jej
    nainstaluje. Obrazy obsahují také Calamares, jednoduchý instalátor, který umožní
    provést instalaci přímo ze spuštěného živého obrazu. Zkušenější uživatelé pak mohou
    využít výchozí Debian instalátor, který nabízí více možností včetně automatické síťové
    instalace.
  </dd>
</dl>

<dl>
  <dt><strong>Debian nabízí bezproblémové aktualizace.</strong></dt>
  <dd>
    Udržovat operační systém aktuální je v našem případě velmi jednoduché, ať už
    chcete provést aktualizaci pouze jednoho balíčku nebo rovnou celého systému.
  </dd>
</dl>

<dl>
  <dt><strong>Debian slouží jako základ pro mnohé další distriuce.</strong></dt>
  <dd>
    Mnoho populárních linuxových distribucí, např. Ubuntu, Knoppix, PureOS nebo Tails,
    je založeno na Debianu. Veškeré nástroje, které jsou potřeba k tomu, aby kdokoli
    mohl rozšířit sadu softwarových balíčků v archivech Debianu o své vlastní balíčky,
    jsou volně k dispozici všem.
  </dd>
</dl>

<dl>
  <dt><strong>Projekt Debian je komunitní.</strong></dt>
  <dd>
    Součástí naší komunity může být kdokoli, není potřeba
    mít znalosti vývojáře či systémového správce. Debian
    je spravován pomocí
    <a href="../devel/constitution">demokratického uspořádání</a>.
    A protože všichni členové Debian projektu mají stejná práva,
    Debian nemůže být ovládnut žádnou komerční společností.
    Naše vývojáře byste našli ve více než šedesáti zemích světa
    a samotný Debian je přeložen do více než osmdesáti jazyků.
  </dd>
</dl>

<aside>
<p><span class="far fa-comment-dots fa-3x"></span> Důvod, proč je Debian považován za systém vhodný pro vývojáře, je velké množství balíčků a podpora softwaru, což je pro vývojáře velmi důležité. Proto je velice vhodný pro softwarové vývojáře a systémové administrátory. <a href="https://fossbytes.com/best-linux-distros-for-programming-developers/">Adarsh Verma na Fossbytes</a></p>
</aside>

<h2><a id="devel">Debian pro vývojáře</a></h2>

<dl>
  <dt><strong>Různé architektury</strong></dt>
  <dd>
    Debian podporuje <a href="../ports">dlouhý seznam</a> CPU architektur,
    včetně amd64, i386, různých verzí ARMu, MIPS, POWER7, POWER8,
    IBM System z a RISC-V. Debian je k dispozici i pro různé okrajové architektury.
  </dd>
</dl>

<dl>
  <dt><strong>IoT a vestavěná zařízení</strong></dt>
  <dd>
    Debian běží na širokém spektru zařízení jako na Raspberry Pi,
    různých variantách QNAPu, mobilních zařízeních, domácích routerech
    i spoustě jednodeskových počítačů.
  </dd>
</dl>

<dl>
  <dt><strong>Obrovské množství softwarových balíčků</strong></dt>
  <dd>
    Debian obsahuje obrovské množství <a
    href="$(DISTRIB)/packages">balíčků</a> (momentálně je ve
    stabilní verzi: <packages_in_stable> balíčků), které jsou zabalené do <a
    href="https://manpages.debian.org/unstable/dpkg-dev/deb.5.en.html">deb
    formátu</a>.
  </dd>
</dl>

<dl>
  <dt><strong>Různé vartianty vydání</strong></dt>
  <dd>
    Mimo stabilní vydání si můžete nainstalovat novější balíčky,
    které najdete ve verzích vydání "testing" anebo "unstable".
  </dd>
</dl>

<dl>
  <dt><strong>Systém sledování chyb</strong></dt>
  <dd>
    Náš systém pro <a href="../Bugs">sledování chyb</a> (bugů) je veřejně
    dostupný jednoduše přes webový prohlížeč. Informace o chybách v našem
    softwaru neschováváme a je velice jednoduché nové chyby nahlásit
    anebo se zapojit do debaty ohledně těch stávajících.
  </dd>
</dl>

<dl>
  <dt><strong>Zásady a nástroje pro vývojáře</strong></dt>
  <dd>
    Debian nabízí velmi kvalitní software. Pokud se chcete dozvědět
    více o našich standardech, přečtěte si stránku o
    <a href="../doc/debian-policy/">zásadách</a>, kde jsou definovány
    technické požadavky kladené na veškeré balíčky, které se nachází
    v naší distribuci. Naše CI strategie je postavena na Autopkgtest
    (spouští testy na balíčcích), Piuparts (testuje proces instalace,
    aktualizace a odinstalace balíču) a Lintian (hledá v balíčcích
    nekonzistence a chyby).
  </dd>
</dl> 

<aside>
<p><span class="far fa-comment-dots fa-3x"></span> Synonymem Debianu je stabilita. [...] Bezpečnost je jedna z nejpodstatnějších vlastností Debianu <a href="https://www.pontikis.net/blog/five-reasons-to-use-debian-as-a-server">Christos Pontikis na pontikis.net</a></p>
</aside>

<h2><a id="enterprise">Debian v podnikovém prostředí</a></h2>

<dl>
  <dt><strong>Debian je spolehlivý.</strong></dt>
  <dd>
    Debian svou spolehlivost dokazuje denodenně v mnoha reálných
    nasazeních, od notebooků po urychlovače částic, burzy a automobilový
    průmysl. Je populární také v akademickém prostoru, ve světě vědy a ve
    veřejném sektoru.
  </dd>
</dl>

<dl>
  <dt><strong>Debian spoléhá na odborníky.</strong></dt>
  <dd>
    Naši správci balíčků nemají na starosti pouhé balení a začleňování
    nových upstreamových verzí. Často jsou také přímo odborníky na danou
    aplikaci a tím pádem mohou přispívat do jejího vývoje přímo
    v upstreamu.
  </dd>
</dl>

<dl>
  <dt><strong>Debian je bezpečný.</strong></dt>
  <dd>
    Pro stabilní verzi Debian nabízí bezpečnostní podporu. Mnohé další
    distribuce a bezpečnostní výzkumníci se spoléhají na náš systém pro sledování
    bepečnostních problémů.
  </dd>
</dl>

<dl>
  <dt><strong>Dlouhodobá podpora (LTS)</strong></dt>
  <dd>
    Debian zdarma nabízí <a href="https://wiki.debian.org/LTS">dlouhodobou podporu</a>
    (LTS), která rozšiřuje standardní dobu života jednoho vydání na nejméně pět let.
    K tomu navíc existuje komerční
    <a href="https://wiki.debian.org/LTS/Extended">prodloužená LTS podpora</a>,
    která udržuje omezenou množinu balíčků aktuální i po dobu delší než pět let.
  </dd>
</dl>

<dl>
  <dt><strong>Obrazy v cloudu</strong></dt>
  <dd>
    Oficiální cloudové obrazy Debianu jsou dostupné u všech hlavních cloudových
    poskytovatelů. Nabízíme také nástroje a konfiguraci potřebnou k tomu, abyste si mohli
    sestavit své vlastní obrazy. Debian můžete používat i ve formě virtuálního stroje
    nebo kontejneru.
  </dd>
</dl>
